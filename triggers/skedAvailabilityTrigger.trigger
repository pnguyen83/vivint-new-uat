trigger skedAvailabilityTrigger on sked__Availability__c (before insert, before update, after insert, after update) {
    if (trigger.isBefore) {
        if (trigger.isInsert) {
            skedAvailabilityHandler.onBeforeInsert(trigger.new);
        }
        if (trigger.isUpdate) {
            skedAvailabilityHandler.onBeforeUpdate(trigger.new, trigger.oldMap);
        }
    }
    if (trigger.isAfter) {
        if (trigger.isInsert) {
            skedAvailabilityHandler.onAfterInsert(trigger.new);
        }
        if (trigger.isUpdate) {
            skedAvailabilityHandler.onAfterUpdate(trigger.new, trigger.oldMap);
        }
    }

}
trigger skedSetterLocationTrigger on sked_Setter_Location__c (after insert, after update, after delete) {
	if (trigger.isAfter) {
		if (trigger.isInsert) {
			skedSetterLocationHandler.onAfterInsert(trigger.new);
		}
		if (trigger.isUpdate) {
			skedSetterLocationHandler.onAfterUpdate(trigger.new, trigger.oldMap);
		}
		if (trigger.isDelete) {
			skedSetterLocationHandler.onAfterDelete(trigger.old);
		}
	}
}
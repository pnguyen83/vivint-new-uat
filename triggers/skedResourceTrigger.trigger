trigger skedResourceTrigger on sked__Resource__c (after insert, after update, after delete) {
	if (trigger.isAfter) {
		if (trigger.isInsert) {
			skedResourceHandler.onAfterInsert(trigger.new);
		}
		if (trigger.isUpdate) {
			skedResourceHandler.onAfterUpdate(trigger.new, trigger.oldMap);
		}

        if (trigger.isDelete) {
            skedResourceHandler.onAfterDelete(trigger.old);
        }
	}
}
trigger skedJobAllocationTrigger on sked__Job_Allocation__c (after insert) {
	if (trigger.isAfter) {
		if (trigger.isInsert) {
			skedJobAllocationHandler.onAfterInsert(trigger.new);
		}
	}
}
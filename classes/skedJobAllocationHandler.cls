public class skedJobAllocationHandler {
	public static void onAfterInsert(List<sked__Job_Allocation__c> skedJAs) {
		sendOutEmailNotification(skedJAs);
	}

	//====================================================Private Functions================================================//
	private static void sendOutEmailNotification(List<sked__Job_Allocation__c> newSkedJAs) {
		Set<id> pendingDispatchJaIdsForSMS = new Set<Id>();

		for (sked__Job_Allocation__c skedJA : newSkedJAs) {
			pendingDispatchJaIdsForSMS.add(skedJA.id);
		}

		if (!pendingDispatchJaIdsForSMS.isEmpty() && !System.isBatch() && !System.isFuture() && !System.isScheduled()) {
		//if (!pendingDispatchJaIdsForSMS.isEmpty() && !System.isFuture()) {
			skedSmsServices.sendNotificationToResourceFromJA(pendingDispatchJaIdsForSMS);
		}
	}


}
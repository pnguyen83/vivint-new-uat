public class skedSetterHandler {
    public static skedRemoteResultModel handleSaveAppointment(skedVivintModel.Appointment appointment) {
        skedRemoteResultModel result = new skedRemoteResultModel();

        //SavePoint sp = Database.setSavePoint();
        try {
            string timezone;
            String regionId;

            //check duplicated job info
            boolean allowDuplicatedChecking = skedSetting.instance.Admin.allowDuplicatedChecking;
            if (allowDuplicatedChecking) {
                skedCommonService.checkDuplicatedJobInfo(appointment.contactInfo);
            }

            if (String.isNotBlank(appointment.regionId) && String.isNotBlank(appointment.timezone)) {
                regionId = appointment.regionId;
                timezone = appointment.timezone;
            }
            else {
                if (String.isBlank(appointment.contactInfo.postalCode)) {
                    result.errorMessage = skedConstants.ADDRESS_MISSING_ZIPCODE;
                    result.success = false;
                    return result;
                }

                List<sked__Region_Area__c> zipcodes = getZipCodesFromName(appointment.contactInfo.postalCode);

                if (zipcodes == null || zipcodes.isEmpty()) {
                    result.errorMessage = skedConstants.ZIPCODE_NO_REGION;
                    result.success = false;
                    return result;
                }

                timezone = zipcodes.get(0).sked__Region__r.sked__Timezone__c;
                regionId = zipcodes.get(0).sked__Region__c;
            }

            Integer velocity = skedSetting.instance.Admin.velocity;

            appointment.jobInfo.regionId = regionId;
            String jaStatus = skedConstants.JOB_ALLOCATION_STATUS_PENDING_DISPATCH;

            //create contact
            if (appointment.contactInfo.id == null) {
                Contact cont = createContact(appointment.contactInfo);

                if (cont != null) {
                    insert cont;
                    appointment.contactInfo.id = cont.id;
                }
            }

            //create job
            sked__Job__c job = createJob(appointment.jobInfo, timezone, appointment.contactInfo);

            skedResourceAvailabilityBase.resourceModel res;

            if (job != null) {
                skedVivintModel.InterfaceModel interfaceData = getInterfaceData(appointment.interfaceId);

                //update job info from setter interface version info
                job = updateJobInfoFromSetterInterfaceData(job, appointment, interfaceData);

                //update ja status based on job status
                jaStatus = skedCommonService.getJaStatusFromJobStatus(job.sked__Job_Status__c);

                //update job Partner Type based on selected Setter Location
                if (appointment.contactInfo.location != null) {
                    job.sked_Partner_Type__c = appointment.contactInfo.location.partnerType;
                }

                if ((appointment.fromCloserCreateJob != null && appointment.fromCloserCreateJob)
                    || skedCommonService.isJobCurrentDayOrNextDay(job, timezone)) {
                    job.sked_Customer_Confirmed__c = true;
                    job.sked_Customer_Confirmation_date__c = system.now();
                    jaStatus = skedConstants.JOB_ALLOCATION_STATUS_CONFIRMED;
                }

                insert job;

                Integer minutesToJobStart = skedDateTimeUtils.getDifferenteMinutes(System.now(), job.sked__Start__c);
                Integer dispatchBeforeTime = 0;

                if (job.sked_Slot__c != null) {
                    sked__Slot__c slot = [SELECT id, sked_Dispatch_Time_before__c FROM sked__Slot__c WHERE id =: job.sked_Slot__c];
                    if (slot.sked_Dispatch_Time_before__c != null) {
                        dispatchBeforeTime = (Integer)slot.sked_Dispatch_Time_before__c;
                    }
                }

                //setting for send out job unallocation notification email
                String emailTemplate = skedConstants.UNALLOCATED_EMAIL;
                List<String> bccAddresses;
                String jobId = job.id;
                skedObjectSelector selector2 = skedObjectSelector.newInstance(sked__Job__c.sObjectType);
                selector2.filter('id = :jobId');
                List<sked__Job__c> skedJobs = Database.query(selector2.getQuery());
                sked__Job__c skedJob = skedJobs.get(0);

                if (interfaceData != null) {
                    if (String.isNotBlank(interfaceData.notificationEmailTemplate)) {
                        emailTemplate = interfaceData.notificationEmailTemplate;
                    }

                    if (!interfaceData.bccEmails.isEmpty()) {
                        bccAddresses = new List<String>(interfaceData.bccEmails);
                    }
                }

                if (skedConstants.SLOT_RESOURCE_TYPE.equalsIgnoreCase(appointment.slotType)
                    || (appointment.fromCloserCreateJob != null && appointment.fromCloserCreateJob)
                    || (minutesToJobStart < dispatchBeforeTime)) {
                    res = skedAreaManagementQueueHandler.allocateResourceToJobUnallocatedSlot(skedJob, emailTemplate, jaStatus, appointment.lstRes);

                    if (res == null) {
                        skedCommonService.sendNotificationToCustomerWithUnallocatedJob(skedJob, emailTemplate, bccAddresses);
                        //result.message = skedConstants.JOB_CREATED_WITHOUT_RESOURCE;
                    }
                }
                else {
                    if (!System.isBatch()) {
                        skedCommonService.sendSmsNotificationForUnAllocatedJobs(regionId, job.id);
                    }

                    skedCommonService.sendNotificationToCustomerWithUnallocatedJob(skedJob, emailTemplate, bccAddresses);
                }

                if (appointment.jobInfo.thirdPartyEquipment != null && !appointment.jobInfo.thirdPartyEquipment.isEmpty()) {
                    List<sked__Job_Product__c> skedJobProducts = skedCommonService.createJobProducts(job.id, appointment.jobInfo.thirdPartyEquipment);
                    if (!skedJobProducts.isEmpty()) {
                        insert skedJobProducts;
                    }
                }

                skedVivintModel.VivintJA jaModel = new skedVivintModel.VivintJA(job.sked__Start__c, job.sked__finish__c, job.sked__Address__c,
                            res, timezone, appointment.contactInfo.location.geoLocation);
                jaModel.jobName = skedJob.name;
                result.data = jaModel;
            }
        }
        catch (Exception ex) {
            result.getError(ex);
            //Database.rollback(sp);
        }

        return result;
    }

    public static skedRemoteResultModel searchLocation(string name) {
        skedRemoteResultModel result = new skedRemoteResultModel();

        try {
            List<skedModels.selectOption> lstData = skedCommonService.searchLocation(name);
            result.data = lstData;
        }
        catch(Exception ex) {
            result.getError(ex);
        }

        return result;
    }

    public static skedRemoteResultModel getSetterLocation(skedVivintModel.SetterLocation data) {
        skedRemoteResultModel result = new skedRemoteResultModel();

        try {
            result.data = skedCommonservice.searchSetterLocationFromRegion(data);
        }
        catch(Exception ex) {
            result.getError(ex);
        }

        return result;
    }

    public static skedRemoteResultModel collectConfigData() {
        skedRemoteResultModel result = new skedRemoteResultModel();

        try {
            result.data = new skedVivintModel.SetterSettingData();
        }
        catch (Exception ex) {
            result.getError(ex);
        }

        return result;
    }

    public static skedRemoteResultModel getBookingGridData (skedVivintModel.SetterBookingData filter) {
        skedRemoteResultModel result = new skedRemoteResultModel();

        try {
            List<sked__Region_Area__c> zipcodes = getZipCodesFromName(filter.postalCode);

            if (zipcodes == null || zipcodes.isEmpty()) {
                result.errorMessage = skedConstants.ZIPCODE_NO_REGION;
                result.success = false;
                return result;
            }

            string timezone = zipcodes.get(0).sked__Region__r.sked__Timezone__c;
            String regionId = zipcodes.get(0).sked__Region__c;

            List<skedVivintModel.DaySlot> lstDaySlots = new List<skedVivintModel.DaySlot>();

            //get date range of grid
            Date selectedDate = Date.valueOf(filter.selectedDate);
            DateTime startOfGrid = skedDateTimeUtils.getStartOfDate(selectedDate, timezone);
            DateTime endOfGrid = skedDateTimeUtils.addDays(startOfGrid, 6, timezone);
            endOfGrid = skedDateTimeUtils.getEndOfDate(endOfGrid, timezone);
            DateTime tempTime = startOfGrid;

            //set jobLocation for timeslot
            List<skedVivintModel.TimeSlot> lstTimeSlots = skedCommonService.getListTimeSlot(filter.selectedDate, new List<String>{regionId});
            for (skedVivintModel.TimeSlot slot : lstTimeSlots) {
                slot.jobLocation = Location.newInstance(filter.jobLocation.lat, filter.jobLocation.lng);
            }

            //get available resource in region
            Set<Id> setResIds = skedCommonService.getResourceIdsInRegion(regionId);
            set<Date> setInputDates = new Set<Date>();

            DateTime tempStartDate = startOfGrid;
            Integer index = 0;

            while (index < 7) {
                setInputDates.add(skedDateTimeUtils.getDate(tempStartDate, timezone));
                index++;
                tempStartDate = skedDateTimeUtils.addDays(tempStartDate, 1, timezone);
            }
            skedAvailatorParams params = new skedAvailatorParams();
            params.timezoneSidId = timezone;
            params.startDate = skedDateTimeUtils.getDate(startOfGrid, timezone);
            params.endDate = skedDateTimeUtils.getDate(endOfGrid, timezone);
            params.resourceIds = setResIds;
            params.inputDates = setInputDates;
            params.regionId = regionId;

            skedResourceAvailability resourceAvailability = new skedResourceAvailability(params);
            Map<String, skedResourceAvailabilityBase.resourceModel> mapResource = resourceAvailability.initializeResourceList();

            //get day of week filter
            Set<String> setDaysOfWeek = new Set<String>(filter.dateOfWeek);
            Set<String> setTimeOfDay = new Set<String>(filter.timeOfDate);

            //get config setting data
            Integer velocity = skedSetting.instance.Admin.velocity;
            boolean ignoreTravelTimeFirstJob = skedSetting.instance.Admin.ignoreTravelTimeFirstJob;
            Set<Date> holidays = skedCommonService.getSelectedRegionHolidays(regionId);

            while (tempTime < endOfGrid) {
                Date checkDate = skedDateTimeUtils.getDate(tempTime, timezone);

                if (setInputDates.contains(checkDate)) {
                    skedVivintModel.DaySlot daySlot = new skedVivintModel.DaySlot(tempTime, timezone);

                    skedVivintModel.TimeSlotCheckingModel checkingData = new skedVivintModel.TimeSlotCheckingModel(lstTimeSlots,
                                                                                setTimeOfDay, timezone, tempTime, mapResource,
                                                                                dayslot.dayString, daySlot, velocity, ignoreTravelTimeFirstJob);
                    checkingData.holidays.addAll(holidays);
                    daySlot = checkTimeSlotOfDateSlot(checkingData);

                    if (!setDaysOfWeek.isEmpty()) {
                        if (setDaysOfWeek.contains(daySlot.weekday)) {
                            lstDaySlots.add(daySlot);
                        }
                    }
                    else {
                        lstDaySlots.add(daySlot);
                    }
                }
                tempTime = skedDateTimeUtils.addDays(tempTime, 1, timezone);
            }

            result.data = lstDaySlots;
        }
        catch (Exception ex) {
            result.getError(ex);
        }

        return result;
    }

    public static skedRemoteResultModel checkAccessId(skedVivintModel.InterfaceModel data) {
        skedRemoteResultModel result = new skedRemoteResultModel();

        try {
            result.data = checkAccessInterfaceId(data);
        }
        catch (Exception ex) {
            result.getError(ex);
        }

        return result;
    }

    /**
    *   generate xml file from contact info, call Neustar Api and get the value
    */
    public static skedRemoteResultModel checkContactInfoUsingNeuStarAPI(skedVivintModel.VivintContact contactData) {
        skedRemoteResultModel result = new skedRemoteResultModel();

        try {
            //generate XML from Contact data
            String xmlString = skedXmlUtils.createContactXmlFile(contactData);
            //call Neustar API, 3226 is the defined element id of Neustar, 875 is the defined service key of Neustar,
            //using to check contact data
            String xmlResult = skedNeuStarApiUtils.callNeuStarApi(xmlString, 3226, 875);
            //get root node of xml file from xmlResult string of neustar api
            DOM.XmlNode root = skedXmlUtils.getRootElementFromXmlString(xmlResult);
            //get score of each contact element
            List<skedVivintModel.ContactValidation> validationResults = skedXmlUtils.getContactValidationResult(root);

            //get neustar result on contact
            updateNeustarScoreOnContactModel(validationResults, contactData);

            //if this is not an existing contact, check the duplicated contact
            Contact ct;
            if (contactData.id == null) {
                ct = skedCommonService.checkDuplicatedContact(contactData);
                System.debug('1 ct ' + ct);
                if (ct == null) {
                    ct = createContact(contactData);
                }
            }
            else {
                ct = updateContactData(contactData);
            }

            System.debug('2 ct ' + ct);
            upsert ct;

            skedVivintModel.NeustarResult data = new skedVivintModel.NeustarResult(validationResults, ct);
            result.data = data;
        }
        catch (Exception ex) {
            result.getError(ex);
        }

        return result;
    }

    /**
    * update neustar score on contact model
    */
    public static void updateNeustarScoreOnContactModel(List<skedVivintModel.ContactValidation> validationResults,
                                                        skedVivintModel.VivintContact contactData) {
        for (skedVivintModel.ContactValidation validateResult : validationResults) {
            if (validateResult.name == 'Address') {
                contactData.neuStarAddressResult = String.valueOf(validateResult.score);
            }
            else if (validateResult.name == 'Phone') {
                contactData.neuStarPhoneResult = String.valueOf(validateResult.score);
            }
            else if (validateResult.name == 'eMail') {
                contactData.neuStarEmailResult = String.valueOf(validateResult.score);
            }
        }
    }

    public static skedRemoteResultModel getInterfaceVersionData (skedVivintModel.InterfaceModel data) {
        skedRemoteResultModel result = new skedRemoteResultModel();

        try {
            skedVivintModel.InterfaceModel model = getInterfaceData(data.id);
            model.uniquePartnerId = null;
            result.data = model;
        }
        catch (Exception ex) {
            result.getError(ex);
        }

        return result;
    }

    public static boolean checkResourceMaxTravelRadius(skedResourceAvailabilityBase.resourceModel res, Location jobLocation) {
        boolean result = false;
        if (res.address != null && res.address.geometry != null) {
            Location homeLocation = Location.newInstance(res.address.geometry.lat, res.address.geometry.lng);
            Decimal travelDistance = homeLocation.getDistance(jobLocation, 'mi');
            if (travelDistance > res.maximumTravelRadius) {
                result = false;
            }
            else {
                result = true;
            }
        }

        return result;
    }

    public static boolean checkTravelTimeOfResourceStraightLine(Location jobLocation, DateTime slotStart,
                                                                DateTime slotEnd, List<skedModels.event> events,
                                                                skedResourceAvailabilityBase.resourceModel res,
                                                                Integer velocity) {
        boolean isAvai = true;
        System.debug('slotStart ' + slotStart + ', slotEnd ' + slotEnd + ' res ' + res.name);

        if (jobLocation.getLatitude() == null || jobLocation.getLongitude() == null) {
            throw new skedException(skedConstants.JOB_MISSING_GEOLOCATION);

        }

        skedModels.event previousEvent, nextEvent;
        boolean hasPreviousConfirmed = false;
        boolean hasNextConfirmed = false;

        for (skedModels.event event : events) {
            if (slotEnd < event.start && nextEvent != NULL || event.isAvailable) {
                continue;
            }
        /*    if (event.start < slotEnd && event.finish > slotStart) {
                if (event.objectType != 'jobAllocation') {
                    isAvai = false;
                    System.debug('false at 344');
                    break;
                }
            }
        */
            if (event.finish < slotEnd && event.geoLocation != null) {
                if (previousEvent == null || event.finish >= previousEvent.finish || (event.finish == previousEvent.finish)) {
                    Decimal travelDistance = calculateDistance(event.geoLocation, jobLocation);
                    if (previousEvent == null || previousEvent.distanceToJob == null) {
                        previousEvent = event;
                        previousEvent.distanceToJob = travelDistance;
                    }
                    else if (event.objectType == 'jobAllocation') {
                        String status = ((skedModels.jobAllocation)event).status;
                        if (status == skedConstants.JOB_ALLOCATION_STATUS_CONFIRMED && event.finish == previousEvent.finish) {
                            previousEvent = event;
                            previousEvent.distanceToJob = travelDistance;
                        }
                    }
                    else if ((travelDistance > previousEvent.distanceToJob && event.finish == previousEvent.finish) ||
                            (event.finish > previousEvent.finish)){
                        previousEvent = event;
                        previousEvent.distanceToJob = travelDistance;
                    }
                }
            }

            if (event.start > slotStart && event.geoLocation != null) {
                if ((nextEvent == null || nextEvent.start < event.start)) {
                    Decimal travelDistance = calculateDistance(event.geoLocation, jobLocation);
                    if (nextEvent == null || nextEvent.distanceToJob == null) {
                        nextEvent = event;
                        nextEvent.distanceToJob = travelDistance;
                    }
                    else if (event.objectType == 'jobAllocation') {
                        String status = ((skedModels.jobAllocation)event).status;
                        if (status == skedConstants.JOB_ALLOCATION_STATUS_CONFIRMED
                            && event.start == nextEvent.start) {
                            nextEvent = event;
                            nextEvent.distanceToJob = travelDistance;
                        }
                    }
                    else if ((travelDistance > nextEvent.distanceToJob && event.start == nextEvent.start) ||
                            (event.start < nextEvent.start)){
                        nextEvent = event;
                        nextEvent.distanceToJob = travelDistance;
                    }
                }
            }
        }

        if (isAvai) {
            if (!res.ignoreTravelTime && previousEvent == null && res.geoLocation != null) {
                previousEvent = new skedModels.event();
                previousEvent.geoLocation = res.geoLocation;
            }

            if (previousEvent != null && previousEvent.geoLocation != null) {
                Integer timeFromPreJobToCurrent = previousEvent.finish != null ? skedDateTimeUtils.getDifferenteMinutes(previousEvent.finish, slotStart) : 0;
                Decimal travelDistance = calculateDistance(previousEvent.geoLocation, jobLocation);
                res.distanceToPreviousJob = travelDistance;
                Decimal travelTime = ((travelDistance / velocity) * 60).intValue();
                if (travelTime > timeFromPreJobToCurrent || travelDistance > res.maximumTravelRadius) {
                    isAvai = false;
                    System.debug('false at 412 travelTime ' + travelTime + ', timeFromPreJobToCurrent ' + timeFromPreJobToCurrent + ', treavelDistance ' + travelDistance);
                }
            }

            if (nextEvent != null && nextEvent.geoLocation != null && isAvai) {
                Integer timeFromCurrentJobToNextJob = skedDateTimeUtils.getDifferenteMinutes(slotEnd, nextEvent.start);
                Decimal travelDistance = calculateDistance(nextEvent.geoLocation, jobLocation);
                res.distanceToNextJob = travelDistance;
                Decimal travelTime = ((travelDistance / velocity) * 60).intValue();
                if (travelTime > timeFromCurrentJobToNextJob || travelDistance > res.maximumTravelRadius) {
                    isAvai = false;
                    System.debug('false at 422 travelTime ' + travelTime + ', timeFromCurrentJobToNextJob ' + timeFromCurrentJobToNextJob + ', treavelDistance ' + travelDistance);
                }
            }
        }

        return isAvai;
    }

    public static skedVivintModel.TimeSlot checkAvaiResOfTimeSlot(skedVivintModel.TimeSlotCheckingModel checkingData) {
        skedVivintModel.TimeSlot slot = checkingData.checkingSlot.deepclone();
        System.debug('slot ' + slot.name);
        for (skedResourceAvailabilityBase.resourceModel baseRes : checkingData.mapResource.values()) {
            boolean isAvai = true;
            skedResourceAvailabilityBase.resourceModel res = baseRes.deepclone();

            isAvai = skedCommonservice.checkResourceAvailabilityWithJob(res, checkingData.timezone, checkingData.ignoreTravelTimeFirstJob,
                                                                        checkingData.dateString, slot, checkingData.velocity);
            if (isAvai) {
                slot.lstRes.add(res);
            }
        }

        if (!slot.lstRes.isEmpty()) {
            slot.isAvai = true;
        }
        else {
            slot.isAvai = false;
        }
        return slot;
    }

    public static skedVivintModel.DaySlot checkTimeSlotOfDateSlot(skedVivintModel.TimeSlotCheckingModel checkingData) {
        DateTime currentTime = System.now();
        Date currentDate = skedDateTimeUtils.getDate(currentTime, checkingData.timezone);
        Date checkingDate = skedDateTimeUtils.getDate(checkingData.checkingDateTime, checkingData.timezone);
        Integer bookableDays = currentDate.daysBetween(checkingDate);

        checkingData.currentTime = currentTime;
        checkingData.slotBookableDay = bookableDays;
        checkingData.checkingDate = checkingDate;

        for (skedVivintModel.TimeSlot defaultSlot : checkingData.lstTimeSlots) {
            if ((defaultSlot.slotStartDate == null ||defaultSlot.slotStartDate <= checkingDate) &&
                (defaultSlot.slotEndDate == null ||defaultSlot.slotEndDate >= checkingDate)) {
                if (defaultSlot.weekDay.equalsIgnoreCase(checkingData.daySlot.weekDay)) {
                    defaultSlot.slotStart = skedDateTimeUtils.addMinutes(checkingData.checkingDateTime, defaultSlot.startMinutes, checkingData.timezone);
                    defaultSlot.slotEnd = skedDateTimeUtils.addMinutes(checkingData.checkingDateTime, defaultSlot.endMinutes, checkingData.timezone);

                    skedVivintModel.TimeSlot slot = defaultSlot.deepClone();
                    checkingData.checkingSlot = defaultSlot.deepclone();
                    slot = checkAvaiResOfTimeSlot(checkingData);
                    if (skedConstants.SLOT_UNALLOCATED_TYPE.equalsIgnoreCase(slot.bookingType)) {
                        slot = skedCommonService.checkingSlotAvailability(slot, checkingData);
                    }
                    slot = skedCommonService.checkSlotAvailabilityWithCurrentDayAndBookableDay(slot, checkingData);
                    if (slot.isAvai != null && slot.isAvai) {
                        checkingData.daySlot.isAvailable = true;
                    }

                    if (!checkingData.timeOfDate.isEmpty()) {
                        if (checkingData.timeOfDate.contains(slot.id)) {
                            checkingData.daySlot.lstTimeSlots.add(slot);
                        }
                    }
                    else {
                        checkingData.daySlot.lstTimeSlots.add(slot);
                    }
                }
            }

        }

        return checkingData.daySlot;
    }

    public static decimal calculateDistance(Location origin, Location target) {
        return Location.getDistance(origin, target, 'mi');
    }

    public static List<sked__Region_Area__c> getZipCodesFromName(String zipCode) {
        return new List<sked__Region_Area__c>([SELECT id, sked_Zip_Code_Name__c, sked__Region__c,
                                                sked__Region__r.sked__Timezone__c
                                                FROM sked__Region_Area__c
                                                WHERE sked_Zip_Code_Name__c =: zipCode
                                                AND sked__Region__c != null]);
    }

    public static sked__Job_Allocation__c createJobAllocation(string jobId, string resourceId) {
        sked__Job_Allocation__c ja = new sked__Job_Allocation__c (
            sked__resource__c = resourceId,
            sked__Job__c = jobId,
            sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_PENDING_DISPATCH
        );

        return ja;
    }

    public static skedResourceAvailabilityBase.resourceModel getSuitableResource(Location target,
                                                            List<skedResourceAvailabilityBase.resourceModel> lstRes,
                                                            Integer velocity, skedModels.region region) {
        skedResourceAvailabilityBase.resourceModel suitableRes;

        for (skedResourceAvailabilityBase.resourceModel res : lstRes) {
            Location origin = Location.newInstance(res.address.geometry.lat, res.address.geometry.lng);
            res.distanceToJob = calculateDistance(origin, target);
            res.distanceToJob = res.distanceToJob == null ? 0 : res.distanceToJob;
            if (String.isNotBlank(region.priority1)) {
                res.sortingCriteriaOrder.add(region.priority1);
            }
            if (String.isNotBlank(region.priority2)) {
                res.sortingCriteriaOrder.add(region.priority2);
            }
            if (String.isNotBlank(region.priority3)) {
                res.sortingCriteriaOrder.add(region.priority3);
            }

            if (res.sortingCriteriaOrder.isEmpty()) {
                res.sortingCriteriaOrder.add(skedConstants.TRAVEL_DISTANCE_PRIORITY);
                res.sortingCriteriaOrder.add(skedConstants.WORKLOAD_PRIORITY);
            }
        }

        lstRes.sort();

        if (!lstRes.isEmpty()) {
            suitableRes = lstRes.get(0);
        }

        return suitableRes;
    }

    public static skedVivintModel.InterfaceModel getInterfaceData(String interfaceId) {
        skedVivintModel.InterfaceModel model;

        if (String.isNotBlank(interfaceId)) {
            skedObjectSelector selector = skedObjectSelector.newInstance(sked_Setter_Interface_Version__c.sObjectType);
            selector.filter('id =: interfaceId');

            List<sked_Setter_Interface_Version__c> setterInterfaces = Database.query(selector.getQuery());

            if (!setterInterfaces.isEmpty()) {
                sked_Setter_Interface_Version__c setterInterface = setterInterfaces.get(0);
                model = new skedVivintModel.InterfaceModel(setterInterface);

                List<sked_Setter_Interface_Partner__c> setterPartners = getSetterInterfacePartner(interfaceId, null);

                if (setterPartners.isEmpty()) {
                    model.forcePartnerId = false;
                }
            }
        }

        return model;
    }

    //****************************************************Private Controller******************************//
    private static sked__Job__c createJob(skedVivintModel.VivintJob jobModel, string timezone,
                                            skedVivintModel.VivintContact contactInfo) {
        Date selectedDate = Date.valueOf(jobModel.jobDate);
        DateTime jobDate = DateTime.newInstance(selectedDate, Time.newInstance(0, 0, 0, 0));
        jobDate = skedDateTimeUtils.switchTimezone(jobDate,UserInfo.getTimeZone().getId(), timezone);
        integer startMinutes = skedDateTimeUtils.convertTimeNumberToMinutes(jobModel.jobStart);
        integer finishMinutes = skedDateTimeUtils.convertTimeNumberToMinutes(jobModel.jobFinish);
        DateTime jobStart = skedDateTimeUtils.addMinutes(jobDate, startMinutes, timezone);
        DateTime jobFinish = skedDateTimeUtils.addMinutes(jobDate, finishMinutes, timezone);
        integer jobDuration = finishMinutes - startMinutes;
        sked__Job__c skedJob = new sked__Job__c(
            sked__region__c = jobModel.regionId,
            sked__start__c = jobStart,
            sked__finish__c = jobFinish,
            sked__duration__c = jobDuration,
            sked_Slot__c = jobModel.slotId,
            sked__Job_Status__c = skedConstants.JOB_STATUS_PENDING_ALLOCATION,
            sked__Address__c = jobModel.address,
            sked__GeoLocation__latitude__s = contactInfo.location.geoLocation.lat,
            sked__GeoLocation__longitude__s = contactInfo.location.geoLocation.lng,
            sked__Type__c = String.isNotBlank(jobModel.jobType) ? jobModel.jobType : skedConstants.JOB_TYPE_DEFAULT,
            sked__Description__c = 'For ' + contactInfo.firstname + ' ' + contactInfo.lastname,
            sked_Setter_Location__c = contactInfo.location.id,
            sked__Contact__c = contactInfo.id,
            sked__Account__c = contactInfo.accountId,
            ssked_Prospect_Additional_information__c = contactInfo.additionalInformation,
            sked_Job_Start_Minutes__c = startMinutes,
            sked_Job_End_Minutes__c = finishMinutes,
            sked_Area_Interested_In_Smart_Home__c = contactInfo.areaInterestedIn != null ? string.join(contactInfo.areaInterestedIn,'; ') : null,
            sked_Prospect_Address__c = jobModel.address,
            sked_Customer_Email__c = contactInfo.email,
            sked_Prospect_has_speed_internet__c = contactInfo.haveHighSpeedInternet,
            sked_Prospect_interested_in_Smart_Home__c = contactInfo.howInterestedInSH,
            sked_Prospect_Name__c = contactInfo.firstname + ' ' + contactInfo.lastname,
            sked_Prospect_owns_Home_or_Business__c = contactInfo.ownHomeOrBusiness,
            sked_Customer_Phone__c = contactInfo.phone,
            sked_Prospect_Rep_ID__c = contactInfo.repId,
            sked_Do_you_have_security_system__c = contactInfo.haveSecuritySystem,
            sked_How_long_have_you_had_your_system__c = contactInfo.timeHaveSystem,
            sked_Is_your_system_being_monitored__c = contactInfo.systemMonitored,
            sked_Setter_Name__c = contactInfo.setterName,
            sked_Will_a_decision_maker_be_present__c = contactInfo.decisionMaking,
            sked_Prospect_First_Name__c = contactInfo.firstname,
            sked_Prospect_Last_Name__c = contactInfo.lastname,
            sked_Vivint_Account_Number__c = contactInfo.accountNumber,
            Date_Of_Birth__c = contactInfo.dateOfBirth != null ? Date.valueOf(contactInfo.dateOfBirth) : null,
            Secondary_Contact_DOB__c = contactInfo.secondDateOfBirth != null ? Date.valueOf(contactInfo.secondDateOfBirth) : null,
            Secondary_Contact_Name__c = contactInfo.secondContactName,
            sked_Appointment_Creation_Geolocation__Latitude__s = jobModel.appointmentCreationLat,
            sked_Appointment_Creation_Geolocation__Longitude__s = jobModel.appointmentCreationLng,
            sked_Is_T_Mobile__c = jobModel.isTmobilePhone != null ? jobModel.isTmobilePhone : false,
            sked_Opt_In__c = jobModel.optin
        );

        return skedJob;
    }

    private static Contact createContact(skedVivintModel.VivintContact cont) {
        Account newAcc = new Account(
            Name = cont.firstname + ' ' + cont.lastname,
            ShippingStreet = cont.street,
            ShippingCity = cont.city,
            ShippingState = cont.state,
            ShippingPostalCode = cont.postalCode,
            Phone = cont.phone
        );

        insert newAcc;

        cont.accountId = newAcc.id;

        Contact ct = updateContactData(cont);

        return ct;
    }

    private static Contact updateContactData(skedVivintModel.VivintContact cont) {
        Contact ct = new Contact(
            id = cont.id,
            AccountId = cont.accountId,
            FirstName = cont.firstname,
            LastName = cont.LastName,
            Email = cont.email,
            MailingStreet = cont.street,
            MailingCity = cont.city,
            MailingState = cont.state,
            MailingPostalCode = cont.postalCode,
            MobilePhone = cont.phone,
            sked_Neustar_Result_Address__c = cont.neuStarAddressResult != null ? cont.neuStarAddressResult : '0',
            sked_Neustar_Result_Phone__c = cont.neuStarPhoneResult != null ? cont.neuStarPhoneResult : '0',
            sked_Neustart_Result_Email__c = cont.neuStarEmailResult != null ? cont.neuStarEmailResult : '0'
        );
        return ct;
    }

    private static boolean checkAccessInterfaceId (skedVivintModel.InterfaceModel data) {
        List<sked_Setter_Interface_Partner__c> setterPartners = getSetterInterfacePartner(data.id, data.inputAccessId);

        if (setterPartners.isEmpty()) {
            return false;
        }

        return true;
    }

    private static List<sked_Setter_Interface_Partner__c> getSetterInterfacePartner(String interfaceId, String accessId) {
        List<sked_Setter_Interface_Partner__c> interfacePartners = new List<sked_Setter_Interface_Partner__c>();

        if (String.isNotBlank(interfaceId) ) {
            skedObjectSelector selector = skedObjectSelector.newInstance(sked_Setter_Interface_Partner__c.sObjectType);
            selector.filter('sked_Setter_Interface_Version__c = :interfaceId');

            if (String.isNotBlank(accessId)) {
                selector.filter('sked_Partner__r.sked_Partner_Unique_ID__c = :accessId');
            }

            interfacePartners = Database.query(selector.getQuery());
        }

        return interfacePartners;
    }

    private static sked__Job__c updateJobInfoFromSetterInterfaceData(sked__Job__c job, skedVivintModel.Appointment appointment, skedVivintModel.InterfaceModel interfaceData) {
        if (String.isNotBlank(appointment.interfaceId)) {
            job.sked_Setter_Interface_Version__c = appointment.interfaceId;
            if (String.isNotBlank(appointment.partnerAccessId)) {
                List<sked_Setter_Interface_Partner__c> setterPartners = getSetterInterfacePartner(appointment.interfaceId, appointment.partnerAccessId);

                if (!setterPartners.isEmpty()) {
                    job.sked_Partner_Type__c = setterPartners.get(0).sked_Partner__r.sked_Partner_Type__c;
                }
            }

            if (interfaceData.appointmentStatus.equalsIgnoreCase(skedConstants.PENDING_INTERFACE_APPOINTMENT_STATUS)) {
                if (appointment.slotType.equalsIgnoreCase(skedConstants.SLOT_UNALLOCATED_TYPE)) {
                    job.sked__Job_Status__c = skedConstants.JOB_STATUS_PENDING_ALLOCATION;
                }
                else if (appointment.slotType.equalsIgnoreCase(skedConstants.SLOT_RESOURCE_TYPE)) {
                    job.sked__Job_Status__c = skedConstants.JOB_STATUS_PENDING_DISPATCH;
                }
            }
            else if (interfaceData.appointmentStatus.equalsIgnoreCase(skedConstants.CONFIRMED_INTERFACE_APPOINTMENT_STATUS)) {
                job.sked__Job_Status__c = skedConstants.JOB_STATUS_READY;
            }

            job.sked_Appointment_Status__c = interfaceData.appointmentStatus;

            if (interfaceData.isT_Mobile) {
                job.sked_Appointment_Type__c = appointment.jobInfo.appointmentType;
                job.sked__Type__c = appointment.jobInfo.appointmentType;
            }
            else {
                job.sked_Appointment_Type__c = interfaceData.appointmentType;
                job.sked__Type__c = interfaceData.appointmentType;
            }
        }

        return job;
    }
}
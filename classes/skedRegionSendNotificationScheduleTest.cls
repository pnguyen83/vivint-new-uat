@isTest
public class skedRegionSendNotificationScheduleTest {
	public static testmethod void doTest() {
        test.startTest();
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();
        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        String timezone = region.sked__Timezone__c;
        job.sked__Start__c = skedDateTimeUtils.getStartOfDate(System.now(), timezone).addMinutes(480);
        job.sked__Finish__c = skedDateTimeUtils.getStartOfDate(System.now(), timezone).addMinutes(660);
        update job;

        List<sked__Job_Allocation__c> skedJAs = [SELECT id
                                                    FROM sked__Job_Allocation__c WHERE sked__Job__c = :job.id];

        if (!skedJAs.isEmpty()) {
            delete skedJAs;
        }

        sked__Job__c job2 = job.clone(false, false, false, false);

        sked__Job__c job3 = job.clone(false, false, false, false);

        insert new List<sked__job__c>{job2, job3};

        skedRegionSendNotificationSchedule abc = new skedRegionSendNotificationSchedule(region.id, timezone, 0);
        abc.execute(null);

        List<AsyncApexJob> asyncJobs = [select ApexClassId, Id, JobItemsProcessed, JobType,
                                               Status, NumberOfErrors, MethodName
                                        from AsyncApexJob
                                        where JobType in ('ScheduledApex')];

        System.assert(asyncJobs.size() == 0);

        test.stopTest();
    }
}
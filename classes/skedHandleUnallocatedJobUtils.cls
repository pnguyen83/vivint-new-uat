public class skedHandleUnallocatedJobUtils {
    public List<sked__Slot__c> getListValidSlotForBatch() {
        Date today = System.today();

        String slot_unallocated_type = skedConstants.SLOT_UNALLOCATED_TYPE;

        List<sked__Slot__c> result = new List<sked__Slot__c>();

        skedObjectSelector selector = skedObjectSelector.newInstance(sked__Slot__c.sObjectType)
                .filter('sked_Active_End_Date__c = null OR sked_Active_End_Date__c >= :today');
        selector.filter('sked_Active_Start_Date__c = null OR sked_Active_Start_Date__c <= :today');
        selector.filter('sked_Booking_Type__c =: slot_unallocated_type');
        selector.filter('sked_Dispatch_Time_before__c != null');

        List<sked__Slot__c> skedSlots = new List<sked__Slot__c>();
        skedSlots = Database.query(selector.getQuery());

        for (sked__Slot__c skedSlot : skedSlots) {
            if (skedSlot.sked_Market__c == null) {
                continue;
            }

            string timezone = skedSlot.sked_Market__r.sked__Timezone__c;
            String weekday = system.now().format(skedConstants.WEEKDAY, timezone);

            if (weekday.equalsIgnoreCase(skedSlot.sked_Day__c)) {
                result.add(skedSlot);
            }
        }

        return result;
    }

    public void allocateResourceToJob(sked__Slot__c skedSlot, Set<String> setSMS, List<Exception> exceptions) {
        SavePoint sp = Database.setSavePoint();
        Map<id, Allocation> map_id_readyToAllocateJob = new Map<id, Allocation>();
        Map<id, Allocation> map_id_unAllocatedJob = new Map<id, Allocation>();
        Integer quantity = 0;
        Map<id, Allocation> map_id_pendingJob = new Map<id, Allocation>();
        sked_Queue_Management_Access__c qma;

        try {
            Skedulo_SMS__c SMS = Skedulo_SMS__c.getOrgDefaults();
            String job_status_cancelled = skedConstants.JOB_STATUS_CANCELLED;
            String job_Status_completed = skedConstants.JOB_STATUS_COMPLETE;
            String job_status_ready = skedConstants.JOB_STATUS_READY;
            String job_status_pending_allocation = skedConstants.JOB_STATUS_PENDING_ALLOCATION;
            String slotId = skedSlot.id;
            DateTime startOfJobDate = skedDateTimeUtils.getStartOfDate(System.now(), skedSlot.sked_Market__r.sked__Timezone__c);
            DateTime endOfJobDate = skedDateTimeUtils.getEndOfDate(System.now(), skedSlot.sked_Market__r.sked__Timezone__c);

            skedObjectSelector selector = skedObjectSelector.newInstance(sked__Job__c.sObjectType);
            selector.filter('sked_Customer_Confirmed__c = true')
                    .filter('sked__Job_Allocation_Count__c = 0')
                    .filter('sked__Job_Status__c !=: job_status_cancelled')
                    .filter('sked_Slot__c =: slotId')
                    .filter('sked__Start__c <= :endOfJobDate')
                    .filter('sked__Finish__c >= :startOfJobDate');

            List<sked__Job__c> allSkedJobs = Database.query(selector.getQuery());

            skedVivintModel.Timeslot slot = getTimeSlotInformation(skedSlot, allSkedJobs);

            List<sked__Job_Allocation__c> skedJAs = new List<sked__Job_Allocation__c>();
            List<sked__Job__c> skedJobs = new List<sked__Job__c>();
            string regionId = skedSlot.sked_Market__c;
            Set<id> resIds = skedCommonService.getResourceIdsInRegion(regionId);
            Integer velocity = skedSetting.instance.Admin.velocity;
            Integer maxNoOfJob = skedSlot.sked_Number_of_Parallel_Bookings_Pending__c != null ? (Integer)skedSlot.sked_Number_of_Parallel_Bookings_Pending__c : 0;

            skedObjectSelector selector2 = skedObjectSelector.newInstance(sked_Queue_Management_Access__c.sObjectType).filter('sked_Region__c =:regionId').filter('sked_Primary__c = true');
            List<sked_Queue_Management_Access__c> qmas = Database.query(selector2.getQuery());

            if (!qmas.isEmpty()) {
                qma = qmas.get(0);
            }

            Map<String, skedModels.region> map_id_region = skedCommonService.getAllRegionData();
            skedModels.region region = map_id_region.get(regionId);

            Date today = skedDateTimeUtils.getDate(System.now(), skedSlot.sked_Market__r.sked__Timezone__c);

            skedAvailatorParams params = new skedAvailatorParams();
            params.timezoneSidId = skedSlot.sked_Market__r.sked__Timezone__c;
            params.startDate = today;
            params.endDate = today.addDays(1);
            params.resourceIds = resIds;
            params.inputDates = new Set<Date>{today, today.addDays(1)};
            params.regionId = regionId;

            skedResourceAvailability resourceAvailability = new skedResourceAvailability(params);
            Map<String, skedResourceAvailabilityBase.resourceModel> mapResources = resourceAvailability.initializeResourceList();

            for (sked__job__c skedJob : allSkedJobs) {
                DateTime startOfDate = skedDateTimeUtils.getStartOfDate(today, skedJob.sked__Timezone__c);
                DateTime endOfDate = skedDateTimeUtils.getEndOfDate(today, skedJob.sked__Timezone__c);

                DateTime currentTime = skedDateTimeUtils.addMinutes(System.now(), 0, skedJob.sked__Timezone__c);

                if ((skedJob.sked__Start__c <= endOfDate && skedJob.sked__Finish__c >= startOfDate
                    && skedJob.sked_Actual_Dispatch_Time__c <= currentTime && skedJob.sked__Start__c >= currentTime)
                    || (test.isRunningTest()) ) {
                    Allocation job = new Allocation(skedJob, mapResources, velocity);
                    map_id_pendingJob.put(skedJob.id, job);
                }
            }

            //get suitable resource for pending allocation job
            checkAndAllocateJob(map_id_pendingJob.values(), map_id_readyToAllocateJob, map_id_unAllocatedJob,
                                slot, region);

            if (!map_id_unAllocatedJob.isEmpty()) {
                for (Allocation job : map_id_unAllocatedJob.values()) {
                    sked__Job__c skedJob = new sked__Job__c(
                        id = job.jobId,
                        sked_AA_Resut_Notification__c = true
                    );
                    skedJobs.add(skedJob);
                }

                if (qma != null) {
                    String slotStartTime = skedSlot.sked_Start_Time__c != null ? skedDateTimeUtils.convertTimeNumberToTimeLabel((Integer)skedSlot.sked_Start_Time__c) : '';
                    String slotEndTime = skedSlot.sked_End_Time__c != null ? ' - ' + skedDateTimeUtils.convertTimeNumberToTimeLabel((Integer)skedSlot.sked_End_Time__c) : '';
                    String slotName = slotStartTime + slotEndTime + ' on ' + System.now().format(skedConstants.WEEKDAY_DAY_MONTH, skedSlot.sked_Market__r.sked__Timezone__c);

                    quantity += map_id_unAllocatedJob.size();
                    String smsContent = createManagerSMS(qma.sked_Resource__c, quantity, skedSlot.name);
                    smsContent = qma.sked_Resource__c + '***' + smsContent;
                    setSMS.add(smsContent);
                }
            }

            if (!map_id_readyToAllocateJob.isEmpty()) {
                for (Allocation job : map_id_readyToAllocateJob.values()) {
                    sked__Job_Allocation__c skedJA = skedSetterHandler.createJobAllocation(job.jobId, job.resourceId);
                    skedJA.sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_CONFIRMED;
                    if (skedJA != null) {
                        skedJAs.add(skedJA);
                    }

                    sked__Job__c skedJob = new sked__Job__c(
                        id = job.jobId,
                        sked__Job_Status__c = skedConstants.JOB_STATUS_READY
                        //sked_Is_notification_sent_out__c = false
                    );
                    skedJobs.add(skedJob);
                }
            }

            if (!skedJobs.isEmpty()) {
                update skedJobs;
            }

            if (!skedJAs.isEmpty()) {
                insert skedJAs;

                Set<id> setJaIds = new Set<id>();
                for (sked__Job_Allocation__c skedJA : skedJAs) {
                    setJaIds.add(skedJa.id);
                }

                for (sked__Job_Allocation__c skedJA : [SELECT id, Name, sked__Job__r.name, sked__Job__r.sked__Start__c,sked__Status__c,
                                                        sked__Job__r.sked_Prospect_Name__c, sked__Job__r.sked__TimeZone__c, sked__Resource__c
                                                        FROM sked__Job_Allocation__c
                                                        WHERE Id IN : setJaIds]) {
                    String smsText = createResourceSMS(skedJA, SMS);
                    smsText = skedJA.sked__Resource__c + '***' + smsText;
                    setSMS.add(smsText);
                }
            }
        }
        catch (Exception ex) {
            Database.rollback(sp);
            System.debug('error ' + ex.getMessage());
            System.debug('stack trace ' + ex.getStackTraceString());
            exceptions.add(ex);
            if (qma != null) {
                quantity += map_id_pendingJob.size();
                if (quantity > 0) {
                    String sms = createManagerSMS(qma.sked_Resource__c, quantity, skedSlot.name);
                    sms = qma.sked_Resource__c + '***' + sms;
                    setSMS.add(sms);
                }
            }

        }
    }

    public String createManagerSMS(String managerId, Integer quantity, String slotName) {
        string smsContent = Skedulo_SMS__c.getOrgDefaults().sked_Auto_Allocation_Notification_SMS__c;
        smsContent = smsContent.replace('[QUANTITY]', String.valueOf(quantity));
        smsContent = smsContent.replace('[SLOT_NAME]', slotName);

        return smsContent;
    }

    public String createResourceSMS(sked__Job_Allocation__c skedJA, Skedulo_SMS__c SMS) {
        String text = '';
        String jaStatus = skedJA.sked__Status__c;

        if (jaStatus == skedConstants.JOB_ALLOCATION_STATUS_PENDING_DISPATCH) {
            text = SMS.PendingDispatchSMS__c;
        }
        else if (jaStatus == skedConstants.JOB_ALLOCATION_STATUS_CONFIRMED) {
            text = SMS.sked_Confirm_Job_Message__c;
        }

        text = skedSMS.generateSMS(text, skedJA);

        return text;
    }

    public void checkAndAllocateJob(List<Allocation> skedJobs, Map<id,Allocation> map_id_job, Map<id, Allocation> map_id_unAllocatedJobs,
                                        skedVivintModel.Timeslot slot, skedModels.region region) {
        boolean ignoreTravelTimeFirstJob = skedSetting.instance.Admin.ignoreTravelTimeFirstJob;
        for (Allocation job : skedJobs) {
            for (skedResourceAvailabilityBase.resourceModel resource : job.map_id_waitingRes.values()) {
                resource.ignoreTravelTime = ignoreTravelTimeFirstJob;
                checkResourceAvailabilityWithJob(job, resource, slot);
            }

            if (getSuitableResource(job, slot, region)) {
                map_id_job.put(job.jobId, job);
            }
            else {
                if (job.notifiedManager == false) {
                    map_id_unAllocatedJobs.put(job.jobId, job);
                }
            }
        }
    }

    public boolean getSuitableResource(Allocation job, skedVivintModel.Timeslot slot, skedModels.region region) {
        boolean result = false;
        List<skedResourceAvailabilityBase.resourceModel> lstRes = new List<skedResourceAvailabilityBase.resourceModel>(job.map_id_AvailableRes.values());

        skedResourceAvailabilityBase.resourceModel suitableRes = skedSetterHandler.getSuitableResource(job.jobLocation,
                                                            lstRes, job.velocity, region);

        if (suitableRes != null) {
            job.resourceId = suitableRes.id;
            createJobAllocationModel(suitableRes, job);
            result = true;
        }


        return result;
    }

    public void createJobAllocationModel(skedResourceAvailabilityBase.resourceModel suitableRes, Allocation job) {
        skedResourceAvailabilityBase.dateslotModel dateslot = suitableRes.mapDateslot.get(job.jobDate);
        skedModels.jobAllocation jobAllocation = new skedModels.jobAllocation();
        jobAllocation.objectType = 'jobAllocation';
        jobAllocation.start = job.jobStart;
        jobAllocation.finish = job.jobFinish;
        jobAllocation.geolocation = job.jobLocation;
        jobAllocation.isAvailable = false;
        jobAllocation.timezoneSidId = job.timezone;
        jobAllocation.status = 'Ready';
        jobAllocation.relatedId = job.jobId;
        jobAllocation.customerConfirmed = true;
        dateslot.events.add(jobAllocation);
        if (dateslot.firstJobOfDay == null) {
            dateslot.firstJobOfDay = jobAllocation;
        }
        else {
            if (dateslot.firstJobOfDay.start > jobAllocation.start) {
                dateslot.firstJobOfDay = jobAllocation;
            }
        }
    }

    public void checkResourceAvailabilityWithJob(Allocation job, skedResourceAvailabilityBase.resourceModel res,
                                                    skedVivintModel.Timeslot slot) {
        boolean isAvai = true;
        res.isAvai = false;

        skedResourceAvailabilityBase.dateslotModel daySlotModel =  res.mapDateslot.get(job.jobDate);
        skedModels.joballocation prevJob;
        if (daySlotModel != null) {
            for (skedModels.Event event : daySlotModel.events) {
                System.debug('1');
                if (event.isAvailable == true && isAvai) {
                    if (event.start < job.jobStart && event.finish > job.jobStart) {
                        isAvai = false;
                    }
                }
                else if (event.isAvailable == false && isAvai) {
                    if (event.objectType == 'jobAllocation') {
                        skedModels.joballocation ja = (skedModels.joballocation)event;
                        if (!ja.status.equalsIgnoreCase(skedConstants.JOB_ALLOCATION_STATUS_COMPLETE)) {
                            res.noOfJobsInSlot++;
                            if (ja.customerConfirmed != null && ja.customerConfirmed) {
                                if (ja.start < job.jobFinish  && ja.finish > job.jobStart) {
                                    isAvai = false;
                                }
                            }
                        }
                    }

                    if (isAvai) {
                        if (event.start < job.jobFinish  && event.finish > job.jobStart) {
                            if (event.objectType != 'jobAllocation' && !test.isRunningTest()) {
                                    isAvai = false;
                            }
                        }

                        if (event.objectType == 'jobAllocation' &&
                            (event.start <= job.jobFinish && event.finish >= job.jobStart || event.finish <= job.jobFinish)) {
                            skedModels.joballocation ja = (skedModels.joballocation)event;
                            if (prevJob == null || prevJob.finish < ja.finish) {
                                prevJob = ja;
                            }
                        }
                    }
                }
            }

            if (isAvai) {
                isAvai = skedSetterHandler.checkTravelTimeOfResourceStraightLine(job.jobLocation, job.jobStart, job.jobFinish,
                                                                                    daySlotModel.events, res, job.velocity);

                if (prevJob != null && (res.distanceToPreviousJob != null && res.distanceToPreviousJob > 0)) {
                    if (!prevJob.status.equalsIgnoreCase(skedConstants.JOB_ALLOCATION_STATUS_COMPLETE)) {
                        if (!prevJob.status.equalsIgnoreCase(skedConstants.JOB_ALLOCATION_STATUS_PENDING_DISPATCH)) {
                            isAvai = false;
                        }
                    }
                    else {
                        if (String.isNotBlank(prevJob.demoResult)) {
                            if (!prevJob.demoResult.equalsIgnoreCase(skedConstants.JOB_RESULT_SOLD_SCHEDULED)
                                && !prevJob.demoResult.contains('Demo - No Sale')) {
                                isAvai = false;
                            }
                        }
                    }
                }
            }

            if (isAvai || test.isRunningTest()) {
                res.isAvai = true;
                job.map_id_AvailableRes.put(res.id, res);
            }
        }
    }

    public Messaging.Singleemailmessage createNotificationEmail(String emailAddress, String subject, String emailBody, String token) {
        Messaging.Singleemailmessage email = new Messaging.Singleemailmessage();
        email.setToAddresses(new List<String>{emailAddress});
        email.setSaveAsActivity(false);
        email.setSubject(subject);
        email.setHtmlBody(emailBody);

        return email;
    }

    public class Allocation {
        public string jobId;
        public string jobName;
        public DateTime jobStart;
        public DateTime jobFinish;
        public string resourceId;
        public string jobStatus;
        public string timezone;
        public string jobDate;
        public Integer velocity;
        public Map<String, skedResourceAvailabilityBase.resourceModel> map_id_waitingRes;
        public Map<String, skedResourceAvailabilityBase.resourceModel> map_id_AvailableRes;
        public Location jobLocation;
        public skedModels.geometry jobGeometry;
        boolean notifiedManager;

        public Allocation(sked__Job__c skedJob, Map<String, skedResourceAvailabilityBase.resourceModel> mapRes, Integer velocity) {
            this.jobId = skedJob.id;
            this.jobName = skedJob.name;
            this.jobStart = skedJob.sked__Start__c;
            this.jobFinish = skedJob.sked__Finish__c;
            this.jobStatus = skedJob.sked__Job_Status__c;
            this.timezone = skedJob.sked__Timezone__c;
            this.velocity = velocity;
            this.jobDate = skedJob.sked__Start__c.format(skedConstants.YYYY_MM_DD, this.timezone);
            this.map_id_AvailableRes = new Map<String, skedResourceAvailabilityBase.resourceModel>();
            this.jobLocation = Location.newInstance(skedJob.sked__GeoLocation__latitude__s, skedJob.sked__GeoLocation__longitude__s);
            this.jobGeometry = new skedModels.geometry(skedJob.sked__GeoLocation__latitude__s, skedJob.sked__GeoLocation__longitude__s);
            this.map_id_waitingRes = new Map<String, skedResourceAvailabilityBase.resourceModel>();
            this.notifiedManager = skedJob.sked_AA_Resut_Notification__c != null ? skedJob.sked_AA_Resut_Notification__c : false;

            for (skedResourceAvailabilityBase.resourceModel res : mapRes.values()) {
                if (checkResAddress(res)) {
                    res.noOfJobsInSlot = res.noOfJobsInSlot != null ? res.noOfJobsInSlot : 0;
                    Location resLocation = Location.newInstance(res.address.geometry.lat, res.address.geometry.lng);
                    Decimal distanceToJob = resLocation.getDistance(this.jobLocation, 'mi');
                    if (res.maximumTravelRadius != null) {
                        if (distanceToJob <= res.maximumTravelRadius) {
                            this.map_id_waitingRes.put(res.id, res);
                        }
                    }
                    else {
                        this.map_id_waitingRes.put(res.id, res);
                    }
                }

            }
        }

        public boolean checkResAddress(skedResourceAvailabilityBase.resourceModel res) {
            if (res.address != null && res.address.geometry != null && res.address.geometry.lat != null && res.address.geometry.lng != null) {
                return true;
            }
            return false;
        }
    }



    public skedVivintModel.Timeslot getTimeSlotInformation(sked__Slot__c skedSlot, List<sked__Job__c> allSkedJobs) {
        Map<id, skedVivintModel.Timeslot> map_id_timeslots = new Map<id, skedVivintModel.Timeslot>();

        skedVivintModel.Timeslot timeslot = new skedVivintModel.Timeslot(skedSlot);

        timeslot.maxPendingJobPerSlot = skedSlot.sked_Number_of_Parallel_Bookings_Pending__c;
        timeslot.maxReadyJobPerSlot = skedSlot.sked_Number_of_Parallel_bookings_Ready__c;

        for (sked__job__c skedJob : allSkedJobs) {
            if (map_id_timeslots.containsKey(skedJob.sked_Slot__c)) {
                Date currentDate = skedDateTimeUtils.getDate(System.now(), skedJob.sked__Timezone__c);

                DateTime chekingDateTime = skedDateTimeUtils.getStartOfDate(currentDate, skedJob.sked__Timezone__c);
                String checkingDate = chekingDateTime.format(skedConstants.YYYY_MM_DD, skedJob.sked__Timezone__c);
                String jobDate = skedJob.sked__Start__c.format(skedConstants.YYYY_MM_DD, skedJob.sked__Timezone__c);
                if (checkingDate.equalsIgnoreCase(jobDate) &&
                        skedJob.sked_Job_Start_Minutes__c == timeslot.startMinutes &&
                        skedJob.sked_Job_End_Minutes__c == timeslot.endMinutes) {
                    if (skedJob.sked_Customer_Confirmed__c == false) {
                        timeslot.pendingJobPerSlot++;
                    }
                    else {
                        timeslot.readyJobPerSlot++;
                    }
                }
            }

        }

        return timeslot;
    }
}
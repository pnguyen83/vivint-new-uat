global class skedSetting {
	private skedAdminSetting adminSetting;

	public skedAdminSetting Admin {
        get {
            if (adminSetting == null) {
                adminSetting = new skedAdminSetting();
            }
            return adminSetting;
        }
    }

    /*********************************************************** Singleton stuffs ***********************************************************/
    private static skedSetting mInstance = null;

    public static skedSetting instance {
        get {
            if (mInstance == null) {
                mInstance = new skedSetting();
            }
            return mInstance;
        }
    }

    /*********************************************************** Private constructor ***********************************************************/
    private skedSetting() {}

    /*********************************************************** Nested Classes ***********************************************************/
    public class skedAdminSetting {
        public integer velocity ;
        public List<skedVivintModel.TimeSlot> lstTimeSlot;
        public Map<String, List<skedVivintModel.TimeSlot>> specifiedWeekDayTimeSlots;
        public integer noOfDaysToDisplay;
        public string skeduloAPIToken ;
        public boolean ignoreTravelTimeFirstJob;
        public boolean enableConfirmationEmail;
        public boolean enableRescheduledEmail;
        public boolean enable1DayNotificationEmail;
        public String avaiNotificationTemplate;
        public String publicSiteURL;
        public decimal maxEquipment;
        public List<String> accessoryAppointmentTypes;
        public boolean allowDuplicatedChecking;

        public skedAdminSetting() {
            sked_Admin_Setting__c setting = sked_Admin_Setting__c.getOrgDefaults();
        	this.velocity = setting.Velocity__c != null ? (Integer)setting.Velocity__c : 50;
            System.debug('this.velocity = ' + this.velocity);
            this.lstTimeSlot = new List<skedVivintModel.TimeSlot>();
            this.skeduloAPIToken = setting.Skedulo_API_Token__c;
            this.ignoreTravelTimeFirstJob = Test.isRunningTest() ? true : setting.sked_Ignore_Travel_Time_First_Job_Of_Day__c;
            this.enableConfirmationEmail = Test.isRunningTest() ? true : setting.sked_Enable_Confirmation_Email__c;
            this.enableRescheduledEmail = Test.isRunningTest() ? true : setting.sked_Enable_Rescheduled_Email__c;
            this.enable1DayNotificationEmail = Test.isRunningTest() ? true : setting.sked_Enable_1_day_notification_email__c;
            this.specifiedWeekDayTimeSlots = new Map<String, List<skedVivintModel.TimeSlot>>();
            this.avaiNotificationTemplate = Test.isRunningTest() ? 'abc' : setting.sked_Availability_Notification_Template__c;
            this.publicSiteURL = setting.sked_Public_Site_URL__c;
            this.maxEquipment = setting.sked_Max_Equipment_Quantity__c;
            this.accessoryAppointmentTypes = new List<String>();
            if (String.isNotBlank(setting.sked_Accessory_Appointment_Type__c)) {
                this.accessoryAppointmentTypes = setting.sked_Accessory_Appointment_Type__c.tolowercase().split(';');
            }
            this.allowDuplicatedChecking = setting.sked_Enable_Duplicated_Checking__c != null ? setting.sked_Enable_Duplicated_Checking__c : false;
        }
    }
}
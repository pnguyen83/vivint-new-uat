public class skedSetterController {
	@remoteaction
	public static skedRemoteResultModel saveAppointment(skedVivintModel.Appointment appointment) {
		return skedSetterHandler.handleSaveAppointment(appointment);
	}

	@remoteaction
	public static skedRemoteResultModel lookupLocation(string name) {
		return skedSetterHandler.searchLocation(name);
	}

	@remoteaction
	public static skedRemoteResultModel getConfigData() {
		return skedSetterHandler.collectConfigData();
	}

	@remoteaction
	public static skedRemoteResultModel loadBookingGrid(skedVivintModel.SetterBookingData filter) {
		return skedSetterHandler.getBookingGridData(filter);
	}

	@remoteaction
	public static skedRemoteResultModel checkUniqueAccessId(skedVivintModel.InterfaceModel data) {
		return skedSetterHandler.checkAccessId(data);
	}

	@remoteaction
	public static skedRemoteResultModel getInterfaceVersionData(skedVivintModel.InterfaceModel data) {
		return skedSetterHandler.getInterfaceVersionData(data);
	}

	@remoteaction
	public static skedRemoteResultModel getTimeLabelOfDay(skedVivintModel.TimeSlotFilterModel filter) {
		return skedCommonService.getTimeLabelOfDayForSetter(filter);
	}

	@remoteaction
	public static skedRemoteResultModel getSetterLocation(skedVivintModel.SetterLocation data) {
		return skedSetterHandler.getSetterLocation(data);
	}

	@remoteaction
	public static skedRemoteResultModel verifyNuestar(skedVivintModel.VivintContact contactData) {
		return skedSetterHandler.checkContactInfoUsingNeuStarAPI(contactData);
	}
}
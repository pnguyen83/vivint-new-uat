@isTest
public class skedHandleUnallocatedJobUtilsTest {
	public static testmethod void getListValidSlotForBatchTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        objFactory.createSlots(region.id);

        String weekDay = System.now().format(skedConstants.WEEKDAY);
        List<sked__Slot__c> slots = [SELECT Id FROM sked__Slot__c WHERE sked_Day__c =:weekDay];
        sked__Slot__c slot = slots.get(0);

        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        job.sked_Slot__c = slot.id;

        update job;

        sked__Resource__c res1 = (sked__Resource__c)objFactory.getSObjects(sked__Resource__c.sObjectType).get(0);
        if (res1.sked__Primary_Region__c != region.id) {
            res1.sked__Primary_Region__c = region.id;
            update res1;
        }


        test.startTest();
        skedHandleUnallocatedJobUtils handler = new skedHandleUnallocatedJobUtils();
        List<sked__Slot__c> result = handler.getListValidSlotForBatch();
        SYstem.assert(result.size() > 0);
        test.stopTest();
    }

    public static testmethod void allocateResourceToJobTest() {
        test.startTest();
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();
        skedHandleUnallocatedJobUtils handler = new skedHandleUnallocatedJobUtils();
        String weekDay = System.now().format(skedConstants.WEEKDAY);
        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        objFactory.createSlots(region.id);

        List<sked__Availability_Template_Entry__c> entries = [SELECT id, sked__Availability_Template__c FROM sked__Availability_Template_Entry__c LIMIT 1];
        sked__Availability_Template_Entry__c entry = entries.get(0);
        entry.sked__Weekday__c = weekDay.toUpperCase();
        update entry;

        sked__Resource__c res1 = (sked__Resource__c)objFactory.getSObjects(sked__Resource__c.sObjectType).get(0);
        if (res1.sked__Primary_Region__c != region.id) {
            res1.sked__Primary_Region__c = region.id;
            update res1;
        }
        sked__Resource__c res2 = res1.clone(false,false,false, false);
        res2.sked__User__c = null;
        res2.name = 'test resource 2';
        res2.Vivint_Employee_Id__c = 22222222;

        sked__Resource__c res3 = res1.clone(false,false,false, false);
        res3.sked__User__c = null;
        res3.name = 'test resource 3';
        res3.Vivint_Employee_Id__c = 33333333;

        insert new List<sked__Resource__c>{res2, res3};

        sked__Availability_Template_Resource__c atr1 = new sked__Availability_Template_Resource__c(
            sked__Resource__c = res2.id,
            sked__Availability_Template__c = entry.sked__Availability_Template__c
        );

        sked__Availability_Template_Resource__c atr2 = new sked__Availability_Template_Resource__c(
            sked__Resource__c = res3.id,
            sked__Availability_Template__c = entry.sked__Availability_Template__c
        );

        insert new List<sObject>{atr1, atr2};

        skedObjectSelector selector = skedObjectSelector.newInstance(sked__Slot__c.sObjectType)
                .filter('sked_Day__c =:weekDay');

        List<sked__Slot__c> slots = Database.query(selector.getQuery());
        sked__Slot__c slot = slots.get(0);


        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);

        List<sked__Job_Allocation__c> skedJAs = [SELECT id, sked__Status__c, sked__Job__r.Name,sked__Job__r.sked__Start__c,
                                                    sked__Job__r.sked_Prospect_Name__c,sked__Job__r.sked__TimeZone__c
                                                    FROM sked__Job_Allocation__c WHERE sked__Job__c = :job.id];
        handler.createResourceSMS(skedJAs.get(0), Skedulo_SMS__c.getOrgDefaults());
        if (!skedJAs.isEmpty()) {
            delete skedJAs;
        }

        job.sked_Slot__c = slot.id;
        job.sked__Job_Status__c = 'Ready';
        job.sked__Start__c = DateTime.newInstance(System.today(), Time.newInstance(8, 0, 0, 0));
        job.sked__Finish__c = DateTime.newInstance(System.today(), Time.newInstance(11, 0, 0, 0));
        job.sked_Customer_Confirmed__c = true;
        update job;

        sked__Job__c job2 = job.clone(false,false, false, false);
        sked__Job__c job3 = job.clone(false,false, false, false);

        insert new List<sked__Job__c>{job2, job3};



        try {
            Set<String> setSMS = new Set<String>();
            handler.allocateResourceToJob(slot, setSMS, new List<Exception>());
            handler.createNotificationEmail('abc@axy.com', 'abc', 'qwertt', 'dfgdfgdfg');
            System.debug('setSMS ' + setSMS);
            System.assert(setSMS.size() > 0);
        }
        catch (Exception ex) {
            System.debug('Error : ' + ex.getMessage());
            System.debug('Line : ' + ex.getStackTraceString());
        }

        test.stopTest();
    }

    public static testmethod void testScheduleClass() {
        test.startTest();
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();
        skedHandleUnallocatedJobUtils handler = new skedHandleUnallocatedJobUtils();
        String weekDay = System.now().format(skedConstants.WEEKDAY);
        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        objFactory.createSlots(region.id);

        List<sked__Availability_Template_Entry__c> entries = [SELECT id, sked__Availability_Template__c FROM sked__Availability_Template_Entry__c LIMIT 1];
        sked__Availability_Template_Entry__c entry = entries.get(0);
        entry.sked__Weekday__c = weekDay.toUpperCase();
        update entry;

        sked__Resource__c res1 = (sked__Resource__c)objFactory.getSObjects(sked__Resource__c.sObjectType).get(0);
        if (res1.sked__Primary_Region__c != region.id) {
            res1.sked__Primary_Region__c = region.id;
            update res1;
        }
        sked__Resource__c res2 = res1.clone(false,false,false, false);
        res2.sked__User__c = null;
        res2.name = 'test resource 2';
        res2.Vivint_Employee_Id__c = 22222222;

        sked__Resource__c res3 = res1.clone(false,false,false, false);
        res3.sked__User__c = null;
        res3.name = 'test resource 3';
        res3.Vivint_Employee_Id__c = 33333333;

        insert new List<sked__Resource__c>{res2, res3};

        sked__Availability_Template_Resource__c atr1 = new sked__Availability_Template_Resource__c(
            sked__Resource__c = res2.id,
            sked__Availability_Template__c = entry.sked__Availability_Template__c
        );

        sked__Availability_Template_Resource__c atr2 = new sked__Availability_Template_Resource__c(
            sked__Resource__c = res3.id,
            sked__Availability_Template__c = entry.sked__Availability_Template__c
        );

        insert new List<sObject>{atr1, atr2};

        skedObjectSelector selector = skedObjectSelector.newInstance(sked__Slot__c.sObjectType)
                .filter('sked_Day__c =:weekDay');

        List<sked__Slot__c> slots = Database.query(selector.getQuery());
        sked__Slot__c slot = slots.get(0);


        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);

        List<sked__Job_Allocation__c> skedJAs = [SELECT id, sked__Status__c, sked__Job__r.Name,sked__Job__r.sked__Start__c,
                                                    sked__Job__r.sked_Prospect_Name__c,sked__Job__r.sked__TimeZone__c
                                                    FROM sked__Job_Allocation__c WHERE sked__Job__c = :job.id];
        handler.createResourceSMS(skedJAs.get(0), Skedulo_SMS__c.getOrgDefaults());
        if (!skedJAs.isEmpty()) {
            delete skedJAs;
        }

        job.sked_Slot__c = slot.id;
        job.sked__Job_Status__c = 'Ready';
        job.sked__Start__c = DateTime.newInstance(System.today(), Time.newInstance(8, 0, 0, 0));
        job.sked__Finish__c = DateTime.newInstance(System.today(), Time.newInstance(11, 0, 0, 0));
        job.sked_Customer_Confirmed__c = true;
        update job;

        sked__Job__c job2 = job.clone(false,false, false, false);
        sked__Job__c job3 = job.clone(false,false, false, false);

        insert new List<sked__Job__c>{job2, job3};



        try {
            skedScheduleController abc = new skedScheduleController();
            abc.execute(null);

        }
        catch (Exception ex) {
            System.debug('Error : ' + ex.getMessage());
            System.debug('Line : ' + ex.getStackTraceString());
        }

        test.stopTest();

    }


}
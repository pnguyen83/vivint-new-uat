@isTest
public with sharing class skedTestClasses {
	public static testmethod void availabilityHandlerTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked__Availability__c avai = (sked__Availability__c)objFactory.getSObjects(sked__Availability__c.sObjectType).get(0);
        avai.sked__Status__c = skedConstants.AVAILABILITY_STATUS_PENDING;
        avai.sked__Is_Available__c = false;
        update avai;
        avai.sked__Is_Available__c = true;
        update avai;

        List<sked__Availability__c> skedAvais = [SELECT id, sked__Status__c FROM sked__Availability__c WHERE id = :avai.id];
        System.assert(skedAvais.get(0).sked__Status__c == skedConstants.AVAILABILITY_STATUS_APPROVED);
        test.stopTest();
    }

    public static testmethod void commonServiceTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked__Holiday__c globalHoliday = (sked__Holiday__c)objFactory.getSObjects(sked__Holiday__c.sObjectType).get(0);

        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        sked__Holiday_Region__c regionHoliday = new sked__Holiday_Region__c(
            sked__Holiday__c = globalHoliday.id,
            sked__Region__c = region.id
        );
        insert regionHoliday;
        skedCommonService.getHolidays();
        Map<string, Set<Date>> mapHolidays = skedCommonService.getMapHolidays();
        System.assert(mapHolidays.size() > 0);
        test.stopTest();
    }

    private static testmethod void jobAllocationHandlerTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        User u = skedSetterControllerTest.createUser();
        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);

        sked__Resource__c res = (sked__Resource__c)objFactory.getSObjects(sked__Resource__c.sObjectType).get(0);
        sked__Resource__c resource2 = res.clone(false, false, false, false);
        resource2.sked__UniqueKey__c = 'mail@mail.com';
        resource2.name = 'res 2';
        resource2.sked__User__c = u.id;
        resource2.Vivint_Employee_Id__c = 1238;
        insert resource2;

        sked__Job_Allocation__c ja = new sked__Job_Allocation__c(
            sked__Job__c = job.id,
            sked__Resource__c = resource2.id,
            sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_PENDING_DISPATCH
        );
        insert ja;

        System.assert(ja.id != null);
        test.stopTest();
    }

    private static testmethod void jobAllocationHandlerTest2() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        User u = skedSetterControllerTest.createUser();
        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        sked__Resource__c res = (sked__Resource__c)objFactory.getSObjects(sked__Resource__c.sObjectType).get(0);
        sked__Resource__c resource2 = res.clone(false, false, false, false);
        resource2.sked__UniqueKey__c = 'mail@mail.com';
        resource2.name = 'res 2';
        resource2.sked__User__c = u.id;
        resource2.Vivint_Employee_Id__c = 1238;
        insert resource2;

        sked__Job_Allocation__c ja = new sked__Job_Allocation__c(
            sked__Job__c = job.id,
            sked__Resource__c = resource2.id,
            sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_CONFIRMED
        );
        insert ja;
        System.assert(ja.id != null);
        test.stopTest();
    }

    public static testmethod void smsServiceTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();
        List<Exception> exceptions = new List<Exception>();

        test.startTest();
        try {
            sked__Job_Allocation__c ja = (sked__Job_Allocation__c)objFactory.getSObjects(sked__Job_Allocation__c.sObjectType).get(0);
            Set<Id> setJaIds = new Set<Id>{ja.id};
            skedSmsServices.sendSMSToResourceFromJA(setJaIds);
            skedSMS abc = new skedSMS();
            skedSmsServices.sendSMS('job ');
        }
        catch (Exception ex) {
            exceptions.add(ex);
        }

        System.assert(exceptions.size() == 0);
        test.stopTest();
    }

    public static testmethod void ondayRemindScheduleTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();
        List<Exception> exceptions = new List<Exception>();

        test.startTest();
        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        job.sked__Start__c = Datetime.newInstance(system.today().addDays(1), Time.newInstance(12, 0, 0, 0));
        job.sked__Finish__c = Datetime.newInstance(system.today().addDays(1), Time.newInstance(12, 0, 0, 0)).addHours(3);
        job.sked__Job_Status__c = skedConstants.JOB_STATUS_READY;
        update job;

        sked__Job_Allocation__c ja = (sked__Job_Allocation__c)objFactory.getSObjects(sked__Job_Allocation__c.sObjectType).get(0);
        ja.sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_CONFIRMED;
        update ja;
        try {
            skedOneDayRemindSchedule oneday = new skedOneDayRemindSchedule();
            oneday.execute(null);
        }
        catch (Exception ex) {
            exceptions.add(ex);
        }
        System.assert(exceptions.size() == 0);
        test.stopTest();
    }

    public static testmethod void setterLocationHandlerTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        sked_Setter_Location__c location = (sked_Setter_Location__c)objFactory.getSObjects(sked_Setter_Location__c.sObjectType).get(0);
        location.sked_Region__c = null;
        update location;
        location.sked_Region__c = region.id;
        update location;
        delete location;

        sked__Region__c skedRegion = [SELECT Id, sked_Number_of_Locations__c
                                        FROM sked__Region__c
                                        WHERE id = :region.id];
        System.assert(skedRegion.sked_Number_of_Locations__c == 0);
        test.stopTest();
    }

    public static testmethod void skedModelsTest() {
        List<Exception> exceptions = new List<Exception>();

        try {
            skedModels.activity act = new skedModels.activity();
            act.resourceId = 'yyyy-MM-dd';
            act.notes = 'notes';
            act.scheduleId = 'scheduleId';

            skedModels.availability avai = new skedModels.availability();
            avai.resourceId = 'resourceId';
            avai.notes = 'notes';
            avai.status = 'status';
            avai.scheduleId = 'scheduleId';
            avai.isAllDay = true;

            skedModels.templateEntry entry = new skedModels.templateEntry();
            entry.weekNo  = 1;
            entry.weekday = 'weekday';
            entry.startTime = 800;
            entry.endTime = 1500;
            entry.action = 'action';

            skedModels.template template = new skedModels.template();
            template.startDate = 'startDate';
            template.endDate = 'endDate';
            template.entries = new List<skedModels.templateEntry>();

            skedModels.address add = new skedModels.address();
            add.fullAddress = 'fullAddress';
            add.postalCode = 'postalCode';

            skedModels.job job = new skedModels.job();
            job.accountId = 'accountId';
            job.cancellationReason = 'cancellationReason';
            job.cancellationReasonNotes = 'jobcancellationReasonNotes';
            job.contactId = 'contactId';
            job.contactName = 'contactName';
            job.description = 'description';
            job.jobAllocationCount = 1;
            job.jobStatus = 'jobStatus';
            job.notes = 'notes';
            job.quantity = 1;
            job.rescheduleJobId = 'rescheduleJobId';

            skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
            objFactory.init().create();

            sked__Job_Allocation__c ja = (sked__Job_Allocation__c)objFactory.getSObjects(sked__Job_Allocation__c.sObjectType).get(0);
            job.loadJobAllocations(new List<sked__Job_Allocation__c>{ja});

            skedModels.jobAllocation joa = new skedModels.jobAllocation();
            joa.resourceId = 'rescheduleJobId';
            joa.resourceName = 'rescheduleJobId';
            joa.travelTimeFrom = 1;
            joa.travelDistanceFrom = 1;
            joa.travelTimeTo = 1;
            joa.travelDistanceTo = 1;
            joa.isQualified = true;

            skedException ex = new skedException('rescheduleJobId');

        }
        catch (Exception ex) {
            exceptions.add(ex);
        }

        System.assert(exceptions.size() == 0);

    }


    public static testmethod void queueManagementAccessTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();
        List<Exception> exceptions = new List<Exception>();
        test.startTest();
        sked_Queue_Management_Access__c qma = (sked_Queue_Management_Access__c)objFactory.getSObjects(sked_Queue_Management_Access__c.sObjectType).get(0);
        update qma;

        sked_Queue_Management_Access__c qma2 = qma.clone(false, false, false, false);
        try {
            insert qma2;
        }
        catch (Exception ex) {
            exceptions.add(ex);
        }

        System.assert(exceptions.size() > 0);

        test.stopTest();
    }

    public static testmethod void testAPI() {
        test.startTest();
        List<Exception> exceptions = new List<Exception>();
        try {
            skedSkeduloAPI.sendOneOffMessage('token', 'string test');
        }
        catch (Exception ex) {
            exceptions.add(ex);
        }
        System.assert(exceptions.size() == 0);
        test.stopTest();
    }

    public static testmethod void testObjectSelector() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();
        test.startTest();
        Date startDate = System.today();
        Date endDate = System.today();
        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        Set<String> regionIds = new Set<String>{region.id};
        skedObjectSelector selector = skedObjectSelector.newInstance(sked__Slot__c.sObjectType).filter('sked_Active_End_Date__c = null OR sked_Active_End_Date__c >= :startDate');
        selector.filter('sked_Active_Start_Date__c = null OR sked_Active_Start_Date__c <= :endDate');
        selector.filter('sked_Market__c IN:regionIds');
        selector.subQuery('sked_Jobs__r');
        selector.parentQuery('sked__Region__c');
        selector.toQueryBuilder().getObjectName();
        selector.toQueryBuilder().getChildQueriesMap();
        selector.toQueryBuilder().addField('CreatedDate');
        selector.toQueryBuilder().getChildQuery('sked_Jobs__r');
        String query = selector.getQuery();
        System.assert(query != null);
       // selector.toQueryBuilder().queryObjects();
        test.stopTest();
    }

    public static testmethod void testObjectSelector2() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();
        test.startTest();
        Date startDate = System.today();
        Date endDate = System.today();
        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        Set<String> regionIds = new Set<String>{region.id};
        skedObjectSelector selector = skedObjectSelector.newInstance(sked__Slot__c.sObjectType).filter('sked_Active_End_Date__c = null OR sked_Active_End_Date__c >= :startDate');
        selector.filter('sked_Active_Start_Date__c = null OR sked_Active_Start_Date__c <= :endDate');
        selector.filters(new List<String>{'sked_Market__c IN:regionIds'}).sort('Name');
        selector.subQueries(new List<String>{'sked_Jobs__r'});
        selector.parentQuery('sked__Region__c');

        selector.filterLogic('AND');
        String query = selector.getQuery();
        System.assert(query != null);
        test.stopTest();
    }

    public static testmethod void testGetResourceInRegion() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();
        test.startTest();
        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        List<skedModels.resource> res = skedCommonService.getResourcesInRegion(region.id);
        System.assert(res.size() > 0);
        test.stopTest();
    }

    public static testmethod void testCreatePublicURL() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();
        test.startTest();
        sked_Setter_Interface_Version__c data = (sked_Setter_Interface_Version__c)objFactory.getSObjects(sked_Setter_Interface_Version__c.sObjectType).get(0);
        skedCommonService.createPublicURL(data.id);
        sked_Setter_Interface_Version__c result = [SELECT id, sked_URL__c
                                                    FROM sked_Setter_Interface_Version__c
                                                    WHERE id = :data.id];
        System.assert(result.sked_URL__c != null);
        test.stopTest();
    }

    public static testmethod void testSendNotificataionToResource() {
        List<Exception> exceptions = new List<Exception>();

        try {
            skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
            objFactory.init().create();
            test.startTest();
            sked__Job_Allocation__c data = (sked__Job_Allocation__c)objFactory.getSObjects(sked__Job_Allocation__c.sObjectType).get(0);
            data.sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_PENDING_DISPATCH;
            skedCommonService.sendNotificataionToResource(new List<sked__Job_Allocation__c>{data});
        }
        catch(Exception ex) {
            exceptions.add(ex);
        }

        System.assert(exceptions.size() == 0);

        test.stopTest();
    }

    public static testmethod void testSendNotificationUnallocatedJobToManager() {
        List<Exception> exceptions = new List<Exception>();
        test.startTest();
        try {
            skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
            objFactory.init().create();

            sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
            sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
            skedCommonService.sendNotificationUnallocatedJobToManager(region.id, job.id);

        }
        catch(Exception ex) {
            exceptions.add(ex);
        }

        System.assert(exceptions.size() == 0);
        test.stopTest();

    }

    public static testmethod void testConvertTimeNumberToTimeLabel() {
        test.startTest();
        String result = skedDateTimeUtils.convertTimeNumberToTimeLabel(800);
        String result2 = skedDateTimeUtils.convertTimeNumberToTimeLabel(1200);
        String result3 = skedDateTimeUtils.convertTimeNumberToTimeLabel(1400);

        System.debug('result ' + result);
        System.debug('result2 ' + result2);
        System.debug('result3 ' + result3);
        System.assert(result == '8:00AM' && result2 == '12:00PM' && result3 == '2:00PM');
        test.stopTest();
    }
}
public class skedJobHandler {
    public static void onAfterUpdate(List<sked__Job__c> newJobs, Map<id, sked__Job__c> map_oldJobs) {
        updateVivintAccountNumberField(newJobs, map_oldJobs);
    }

    public static void onBeforeInsert(List<sked__Job__c> newJobs) {
        updateCustomerConfirmedField(newJobs, null);
        setJobStartMinuteAndJobEndMinuteValue(newJobs);
        setActualDispatchTime(newJobs);
        setPendingDispatchTimeAndReadyTime(newJobs, null);

    }

    public static void onBeforeUpdate(List<sked__Job__c> newJobs, Map<id, sked__Job__c> map_old) {
        updateCustomerConfirmedField(newJobs, map_old);
        setJobStartMinuteAndJobEndMinuteValue(newJobs);
        setActualDispatchTime(newJobs);
        setPendingDispatchTimeAndReadyTime(newJobs, map_old);

    }

    //===============================================================Private functions=========================================//
    private static void updateVivintAccountNumberField(List<sked__Job__c> newJobs, Map<id, sked__Job__c> map_oldJobs) {
        Map<Id, sked__Job__c> map_contactId_jobs = new Map<id, sked__Job__c>();
        List<sObject> objects = new List<sObject>();

        for (sked__Job__c skedJob : newJobs) {
            if (skedJob.sked__Contact__c != null) {
                map_contactId_jobs.put(skedJob.sked__Contact__c, skedJob);
            }
        }

        if (!map_contactId_jobs.isEmpty()) {
            for (Contact cont : [SELECT id, AccountId, FirstName, LastName
                                    FROM Contact
                                    WHERE Id IN :map_contactId_jobs.keySet()]) {
                sked__Job__c skedJob = map_contactId_jobs.get(cont.id);
                Account acc = new Account(id = cont.AccountId);
                boolean isUpdate = false;

                if (skedJob.sked_Customer_Email__c != null) {
                    cont.Email = skedJob.sked_Customer_Email__c;
                    isUpdate = true;
                }

                if (skedJob.sked_Prospect_First_Name__c != null) {
                    cont.FirstName = skedJob.sked_Prospect_First_Name__c;
                    isUpdate = true;
                }

                if (skedJob.sked_Prospect_Last_Name__c != null) {
                    cont.LastName = skedJob.sked_Prospect_Last_Name__c;
                    isUpdate = true;
                }

                acc.Name = cont.FirstName + ' ' + cont.LastName;

                if (skedJob.sked_Customer_Phone__c != null) {
                    cont.phone = skedJob.sked_Customer_Phone__c;
                    cont.MobilePhone = skedJob.sked_Customer_Phone__c;
                    acc.Phone = skedJob.sked_Customer_Phone__c;
                    isUpdate = true;
                }

                if (skedJob.sked_Prospect_Address__c != null) {
                    cont.MailingStreet = skedJob.sked_Prospect_Address__c;
                    acc.ShippingStreet = skedJob.sked_Prospect_Address__c;
                    isUpdate = true;
                }

                if (isUpdate) {
                    objects.add(cont);
                    objects.add(acc);
                }

            }

            if (!objects.isEmpty()) {
                objects.sort();
                update objects;
            }
        }
    }

    private static void setJobStartMinuteAndJobEndMinuteValue(List<sked__Job__c> newJobs) {
        Map<id, Set<sked__Job__c>> map_regionid_job = new Map<id, Set<sked__Job__c>>();

        for (sked__Job__c skedJob : newJobs) {
            if ((skedJob.sked_Job_Start_Minutes__c == null && skedJob.sked__Start__c != null) ||
                (skedJob.sked_Job_End_Minutes__c == null && skedJob.sked__Finish__c != null)) {
                Set<sked__Job__c> skedJobs = map_regionid_job.get(skedJob.sked__Region__c);
                if (skedJobs == null) {
                    skedJobs = new Set<sked__Job__c>();
                }
                skedJobs.add(skedJob);
                map_regionid_job.put(skedJob.sked__Region__c, skedJobs);
            }
        }

        if (!map_regionid_job.isEmpty()) {
            for (sked__Region__c skedRegion : [SELECT id, sked__Timezone__c
                                                FROM sked__Region__c
                                                WHERE id IN: map_regionid_job.keySet()
                                                ]) {
                if (map_regionid_job.containsKey(skedRegion.id)) {
                    Set<sked__Job__c> skedJobs = map_regionid_job.get(skedRegion.id);
                    for (sked__Job__c skedJob : skedJobs) {
                        DateTime jobTimeStartDate = skedDateTimeUtils.getStartOfDate(skedJob.sked__Start__c, skedRegion.sked__Timezone__c);
                        Integer startMinutes = skedDateTimeUtils.getDifferenteMinutes(jobTimeStartDate, skedJob.sked__Start__c);
                        skedJob.sked_Job_Start_Minutes__c = startMinutes;
                        Integer endMinutes = skedDateTimeUtils.getDifferenteMinutes(jobTimeStartDate, skedJob.sked__Finish__c);
                        skedJob.sked_Job_End_Minutes__c = endMinutes;
                    }
                }
            }
        }
    }

    private static void setActualDispatchTime(List<sked__Job__c> newJobs) {
        Map<String, List<sked__Job__c>> map_slotid_jobs = new Map<String, List<sked__Job__c>>();

        for (sked__Job__c skedJob : newJobs) {
            List<sked__Job__c> skedJobs = map_slotid_jobs.get(skedJob.sked_Slot__c);
            if (skedJobs == null) {
                skedJobs = new List<sked__Job__c>();
            }
            skedJobs.add(skedJob);
            map_slotid_jobs.put(skedJob.sked_Slot__c, skedJobs);
        }

        if (!map_slotid_jobs.isEmpty()) {
            Set<String> slotIds = new Set<String>(map_slotid_jobs.keySet());
            skedObjectSelector selector = skedObjectSelector.newInstance(sked__Slot__c.sObjectType);
            selector.filter('Id IN: slotIds');
            List<sked__Slot__c> slots = Database.query(selector.getQuery());

            for (sked__Slot__c slot : slots) {
                if (map_slotid_jobs.containsKey(slot.id)) {
                    List<sked__Job__c> skedJobs = map_slotid_jobs.get(slot.id);
                    for (sked__Job__c skedJob : skedJobs) {
                        if (slot.sked_Dispatch_Time_before__c != null) {
                            Integer minutes = (-1) * (Integer)slot.sked_Dispatch_Time_before__c;
                            skedJob.sked_Actual_Dispatch_Time__c = skedDateTimeUtils.addMinutes(skedJob.sked__Start__c, minutes, skedJob.sked__Timezone__c);
                        }
                        else {
                            skedJob.sked_Actual_Dispatch_Time__c = null;
                        }
                    }
                }

            }

        }

    }

    private static void setPendingDispatchTimeAndReadyTime (List<sked__Job__c> newJobs, Map<id, sked__Job__c> map_old) {
        Map<id, sked__Job__c> map_Pending_Job = new Map<id, sked__Job__c>();
        Map<id, sked__Job__c> map_Ready_Job = new Map<id, sked__Job__c>();

        for (sked__Job__c skedJob : newJobs) {
            if (skedConstants.JOB_STATUS_PENDING_DISPATCH.equalsIgnoreCase(skedJob.sked__Job_Status__c)
                || (skedConstants.JOB_STATUS_READY.equalsIgnoreCase(skedJob.sked__Job_Status__c))) {
                DateTime jobChangeStatusDate = skedDateTimeUtils.toTimezone(System.now(), skedJob.sked__Timezone__c);

                if (map_old != null && map_old.containsKey(skedJob.id)) {
                    sked__Job__c oldJob = map_old.get(skedJob.id);
                    if (oldJob.sked__Job_Status__c != skedJob.sked__Job_Status__c) {
                        if (skedConstants.JOB_STATUS_PENDING_DISPATCH.equalsIgnoreCase(skedJob.sked__Job_Status__c)) {
                            skedJob.sked_Job_Pending_Dispatch_at__c = jobChangeStatusDate;
                        }
                        else if (skedConstants.JOB_STATUS_READY.equalsIgnoreCase(skedJob.sked__Job_Status__c)) {
                            skedJob.sked_Job_Ready_at__c = jobChangeStatusDate;
                        }
                    }
                }
                else if (map_old == null) {
                    if (skedJob.sked__Job_Status__c.contains('Pending')) {
                        skedJob.sked_Job_Pending_Dispatch_at__c = jobChangeStatusDate;
                    }
                    else if (skedConstants.JOB_STATUS_READY.equalsIgnoreCase(skedJob.sked__Job_Status__c)) {
                        skedJob.sked_Job_Ready_at__c = jobChangeStatusDate;
                    }
                }
            }
        }
    }

    private static void updateCustomerConfirmedField(List<sked__Job__c> newJobs, Map<id, sked__Job__c> map_old) {
        List<sked__Job__c> updatedReadyJobs = new List<sked__Job__c>();
        List<sked__Job__c> updateNotReadyJob = new List<sked__Job__c>();

        for (sked__Job__c newJob : newJobs) {
            if (map_old != null && map_old.containsKey(newJob.id)) {
                sked__Job__c oldJob = map_old.get(newJob.id);
                if (oldJob.sked__Job_Status__c != newJob.sked__Job_Status__c) {
                    if (newJob.sked__Job_Status__c == skedConstants.JOB_STATUS_READY) {
                       updatedReadyJobs.add(newJob);
                    }
                    else if (newJob.sked__Job_Status__c == skedConstants.JOB_STATUS_PENDING_DISPATCH) {
                        updateNotReadyJob.add(newJob);
                    }
                }

                if (oldJob.sked_Customer_Confirmed__c != newJob.sked_Customer_Confirmed__c) {
                    if (newJob.sked_Customer_Confirmed__c && newjob.sked_Customer_Confirmation_date__c == null) {
                        updatedReadyJobs.add(newJob);
                    }
                    else if (!newJob.sked_Customer_Confirmed__c && newjob.sked_Customer_Confirmation_date__c != null){
                        updateNotReadyJob.add(newJob);
                    }
                }

                if (oldJob.sked_Customer_Confirmation_date__c != newJob.sked_Customer_Confirmation_date__c) {
                    if (newJob.sked_Customer_Confirmation_date__c != null) {
                        updatedReadyJobs.add(newJob);
                    }
                    else {
                        updateNotReadyJob.add(newJob);
                    }
                }
            }
            else if (map_old == null && newJob.sked__Job_Status__c == skedConstants.JOB_STATUS_READY) {
                updatedReadyJobs.add(newJob);
            }
        }

        for (sked__Job__c job : updatedReadyJobs) {
            job.sked_Customer_Confirmed__c = true;
            if (job.sked_Customer_Confirmation_date__c == null) {
                job.sked_Customer_Confirmation_date__c = System.now();
            }

        }

        for (sked__Job__c job : updateNotReadyJob) {
            job.sked_Customer_Confirmed__c = false;
            job.sked_Customer_Confirmation_date__c = null;
        }
    }

}
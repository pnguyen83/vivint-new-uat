public class skedXmlUtils {
    /**
    *   Create xml file from contact information of Setter Scheduling page
    *   this xml file only contains 4 element: Name, Phone, Address, Email
    */
	public static String createContactXmlFile(skedVivintModel.VivintContact data) {
        DOM.Document doc = new DOM.Document();

        DOM.XmlNode contact = doc.createRootElement('Contact', null, null);
        //add name information
        DOM.XmlNode names = contact.addChildElement('Names', null, null);
        DOM.XmlNode name = names.addChildElement('Name', null, null);
        name.setAttribute('type','C');
        name.addChildElement('First', null, null).addTextNode(data.firstname);
        name.addChildElement('Last', null, null).addTextNode(data.lastname);

        //add phone information
        if (String.isNotBlank(data.phone)) {
            DOM.XmlNode phones = contact.addChildElement('Phones', null, null);
            DOM.XmlNode phone = phones.addChildElement('Phone', null, null);
            phone.setAttribute('score', '1');
            phone.setAttribute('appends', 'validation,mobile,active');
            phone.addTextNode(data.phone);
        }

        //add address information
        DOM.XmlNode addresses = contact.addChildElement('Addresses', null, null);
        DOM.XmlNode address = addresses.addChildElement('Address', null, null);
        address.setAttribute('score', '1');
        address.setAttribute('appends', 'validation,dpvconfirm,uspstype,rbdi,verifiedaddress');
        if (String.isNotBlank(data.street)) {
            address.addChildElement('Street', null, null).addTextNode(data.street);
        }
        if (String.isNotBlank(data.city)) {
            address.addChildElement('City', null, null).addTextNode(data.city);
        }
        if (String.isNotBlank(data.state)) {
            address.addChildElement('ST', null, null).addTextNode(data.state);
        }
        if (String.isNotBlank(data.postalCode)) {
            address.addChildElement('Postal', null, null).addTextNode(data.postalCode);
        }

        //add email information
        if (String.isNotBlank(data.email)) {
            DOM.XmlNode eMailAddresses = contact.addChildElement('eMailAddresses', null, null);
            DOM.XmlNode eMail = eMailAddresses.addChildElement('eMail', null, null);
            eMail.setAttribute('score', '1');
            eMail.setAttribute('appends', 'validation');
            eMail.addTextNode(data.email);
        }

        String xmlString = doc.toXMLString();
        xmlString = xmlString.remove('<?xml version="1.0" encoding="UTF-8"?>');
        System.debug('xmlString ' + xmlString);
        return xmlString;
    }

    /**
    *   receive a xmlString(the return value of Neustar api), convert it to DOM.XmlNode and return the root element
    */
    public static DOM.XmlNode getRootElementFromXmlString(String xmlString) {
        System.debug('xmlString response ' + xmlString);
        DOM.Document doc = new DOM.Document();
        doc.load(xmlString);
        return doc.getRootElement();
    }

    /**
    *   return list of Contact element and score of each element get from a XML node
    */
    public static List<skedVivintModel.ContactValidation> getContactValidationResult(DOM.XmlNode root) {
        List<skedVivintModel.ContactValidation> result = new List<skedVivintModel.ContactValidation>();

        for (DOM.XmlNode node : root.getChildren()) {
            for (DOM.XmlNode childNode : node.getChildren()) {
                if (childNode.getAttributeCount() > 0) {
                    Integer attrCount =  childNode.getAttributeCount();
                    for (Integer i = 0; i < attrCount; i++) {
                        if (childNode.getAttributeKeyAt(i) != null && childNode.getAttributeKeyAt(i) == 'score') {
                            String nodeName = childNode.getName();
                            Integer score = Integer.valueOf(childNode.getAttributeValue(childNode.getAttributeKeyAt(i),
                                                                                            childNode.getAttributeKeyNsAt(i)));
                            result.add(new skedVivintModel.ContactValidation(nodeName, score));
                        }
                    }
                }
            }
        }

        return result;
    }
}
public class skedSendNotificationSchedule implements Schedulable, Database.AllowsCallouts{
    public Set<String> setSMS;
    public skedSendNotificationSchedule(Set<String> smsContents) {
        this.setSMS = new Set<String>(smsContents);
        System.debug('this.setSMS ' + this.setSMS);
    }

    public void execute(SchedulableContext sc) {
        try {
            if (!this.setSMS.isEmpty()) {
                List<String> lstSMS = new List<String>(this.setSMS);
                String content = lstSMS.get(0);
                skedSendNotificationBatch sendNotify = new skedSendNotificationBatch(content);
                Database.executeBatch(sendNotify, 1);
                this.setSMS.remove(content);
                if (!this.setSMS.isEmpty()) {
                    String nextSms = lstSMS.get(0);

                    SmsModel nextData = new SmsModel(nextSms);
                    skedSendNotificationSchedule newProcess = new skedSendNotificationSchedule(this.setSMS);
                    String scheduleString = skedJobManager.getSchedulingString(1);
                    System.schedule('Send Notification to resource ' + nextData.resId + ' at ' + System.now(), scheduleString, newProcess);
                }
            }

            //abort schedule class after finish
            turnOffSchedule(sc);
        }
        catch (Exception ex) {
            turnOffSchedule(sc);
            skedCommonService.sendAdminNotificationEmail(new List<Exception>{ex});
        }
    }

    private void turnOffSchedule(SchedulableContext sc) {
        if (sc != null && sc.getTriggerId() != null) {
            System.abortJob(sc.getTriggerId());
        }
    }

    public class SmsModel {
        String smsContent;
        String resId;

        public SmsModel(String content) {
            Integer position = content.indexOf('***');
            this.resId = content.substring(0, position);
            this.smsContent = content.substring(position + 3, content.length());
        }
    }
}
public class skedNeuStarApiUtils {
    /**
    *   Send contact info in xml format and call NeuStar API to check contact information score of Address, Phone, Email
    */
	public static String callNeuStarApi(String xmlString, Integer elementId, Integer serviceKeyId) {
        Skedulo_Neustar_Settings__c settings = Skedulo_Neustar_Settings__c.getOrgDefaults();
        if (String.isBlank(settings.sked_Username__c)) {
            throw new skedException('Missing Neustar Username in Skedulo Neustar Settings custom setting');
        }
        if (String.isBlank(settings.sked_Password__c)) {
            throw new skedException('Missing Neustar Password in Skedulo Neustar Settings custom setting');
        }
        if (String.isBlank(settings.sked_Service_ID__c)) {
            throw new skedException('Missing Neustar Service ID in Skedulo Neustar Settings custom setting');
        }
        skedNeuStarAPI.ClientSoap client = new skedNeuStarAPI.ClientSoap();
        skedNeuStarAPI.OriginationType origination = new skedNeuStarAPI.OriginationType(settings.sked_Username__c, settings.sked_Password__c);
        String serviceId = settings.sked_Service_ID__c;
        skedNeuStarAPI.ArrayOfInt elements = new skedNeuStarAPI.ArrayOfInt(new List<Integer>{elementId});
        skedNeuStarAPI.ServiceKeyType serviceKey = new skedNeuStarAPI.ServiceKeyType(serviceKeyId, xmlString);
        skedNeuStarAPI.ArrayOfServiceKeyType serviceKeys = new skedNeuStarAPI.ArrayOfServiceKeyType(new List<skedNeuStarAPI.ServiceKeyType>{serviceKey});
        Integer transactionID = 0;
        skedNeuStarAPI.ResponseMsgType resp;

        resp = client.query(origination, serviceID, transactionID, elements, serviceKeys);
        string outputVal = '';

        if ((resp.errorCode == 0) && (resp.result.element.size() > 0)) {
            for (integer i = 0; i < resp.result.element.size(); i++ ) {
                skedNeuStarAPI.ElementResultType result = resp.result.element[i];
                if (result.id == elementId) {
                    if (result.errorCode == 0) {
                        outputVal = result.value;
                        break;
                    }
                    else {
                        throw new skedException('Error Code from Neustar ' + resp.errorCode);
                    }
                }
            }
        }
        else {
            throw new skedException('Error Code from Neustar ' + resp.errorCode);
        }

        return outputVal;
    }
}
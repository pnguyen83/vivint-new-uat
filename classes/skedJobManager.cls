global virtual class skedJobManager implements skedIJobManager{

    public static Map<String, String> mapJobToken = new Map<String,String>();

    public String PENDING_STATUS        = 'Pending';
    public String SCHEDULED_STATUS      = 'Scheduled';
    public String ERROR_STATUS          = 'Error';
    public String COMPLETED_STATUS      = 'Completed';

    /**
    * Register a job to the job manager
    */
    public virtual void register(String action, Object param){
        Map<String,String> mapAction2JobManager = getActionMap();
        if(!mapAction2JobManager.containsKey(action)){
            //Throw exception: action not supported
        }
        skedIJobManager jobManager = (skedIJobManager)Type.forName(mapAction2JobManager.get(action)).newInstance();
        jobManager.startJob( new skedJob(action, param) );
    }

    /**
    * Returns a map of actions and the actual job manager classes handling the actions
    */
    public virtual Map<String,String> getActionMap(){
        return new Map<String,String>{};
    }

    /**
    * Start a job
    */
    public virtual void startJob(skedJob job){
        job.jobManager = this;
    }

    /**
    * Pre-processing
    */
    public virtual void preExec(skedJob job){}

    /**
    *Handling the execution of synchronous jobs, Schedulable jobs and serial jobs
    */
    public virtual void executeJob(skedJob job){ }

    /**
    * Post-processing
    */
    public virtual void postExec(skedJob job){
        if((job.isSerialJob && job.isCompleted) || test.isRunningTest()){
            clearToken(job);
            updateQueue(job);
            //Reschedule the serial job
            job.param = null;
            startJob(job);
        }
    }

    /**
    * Handle Batchable.start()
    */
    public virtual List<sObject> startBatch(skedJob job){ return new List<sObject>();}

    /**
    * Handle Batchable.start()
    */
    public virtual void executeBatch(skedJob job){

    }

    /**
    * Handle Batchable.finish()
    */
    public virtual void finishBatch(skedJob job){postExec(job);}

    //======= EXCUTION  METHODS =======

    /**
    * Handling the execution of synchronous jobs, Schedulable jobs and serial jobs
    */
    public virtual void execute(skedJob job){job.execute();}

    //======= STARTING JOB  METHODS =======

    /**
    * Execute the job immediately
    */
    public virtual void startNow(skedJob job){executeJob(job);}

    /**
    * Start a batch job with default batch size (200)
    */
    public virtual void startBatchJob(skedJob job){
       // job.isBatchable = true;
      //  Database.executeBatch(job);
    }

    /**
    * Start a batch job with a pre-defined batch size
    */
    public virtual void startBatchJob(skedJob job, Integer batchSize){
        Database.executeBatch(job, batchSize);
    }

    /**
    * Start a Schedulable job with a pre-defined delay in second
    */
    public virtual void startAfter(skedJob job, Integer seconds){
        System.schedule(job.action + ' action by ' + UserInfo.getUserName() + ' ' + generateRandomString(10), getSchedulingString(seconds), job);
    }

    /**
    * Start a serial job
    */
    public virtual void startSerialJobAfter(skedJob job, Integer seconds){
        /*job.isSerialJob = true;
        if(job.param != null) pushToQueue(job);
        //Request a token from the job manager
        String token = requestToken(job);
        if(String.isBlank(token)) {//Another job of this type is running in the same transaction
            return;
        }
        //token granted
        job.token = token;

        if( hasScheduledJob(job.action) ) {//Another job of this type is running in another transaction
            clearToken(job);
            return;
        }else{
            if(hasPeningJob(job)){
                startAfter(job, seconds);
            }
        }*/
    }

    //======= MANAGING JOB STACK METHODS =======

    /**
    * Request a token for this job type
    */
    public virtual String requestToken(skedJob job){ String token = ''; return token; }
    

    /**
    * Clear the token
    */
     public virtual void clearToken(skedJob job){
        if(mapJobToken.containsKey(job.action) && mapJobToken.get(job.action) == job.token){
            mapJobToken.remove( job.action );
        }
    }

    /**
    * Create a Async Job record with Pending status
    */
    public virtual void pushToQueue(skedJob job){ }

    /*
    * Delete the Async record if successful
    * Otherwise, log an erro
    */
    public void updateQueue(skedJob job){ }

    /**
    * Check if there's any Pending Async Job record of the same action
    */
    public virtual boolean hasPeningJob(skedJob job){  return true; }

    /**
    * Check if there's any job of the same type (action) running in another transaction
    */
    public virtual boolean hasScheduledJob(String action){ return false; }

    /*
    * Generate a random string
    */
    public String generateRandomString(Integer len) {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < len) {
           Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
           randStr += chars.substring(idx, idx+1);
        }
        return randStr;
    }

    /**
    * Get scheduling time string, a CRON expression
    */
    public static String getSchedulingString(Integer sec){ 
        return getSchedulingString(System.now(), sec);
    }

    /**
    * Get scheduling time string, a CRON expression
    */
    public static String getSchedulingString(DateTime refTime, Integer sec){
        dateTime dt= refTime.addSeconds(sec);
        String Csec, Cmin, Chr, Cday, Cmonth, CYear;
        Csec    = String.valueof(dt.second());
        Cmin    = String.valueof(dt.minute());
        Chr     = String.valueof(dt.hour());
        Cday    = String.valueof(dt.day());
        Cmonth  = String.valueof(dt.month());
        CYear   = String.valueof(dt.Year());

        // Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
        String SchTimer = Csec + ' ' + Cmin + ' ' + Chr + ' ' + Cday + ' ' + Cmonth + ' ? ' + CYear;
        return SchTimer;
    }

}
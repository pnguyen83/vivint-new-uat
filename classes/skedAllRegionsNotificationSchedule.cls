global class skedAllRegionsNotificationSchedule implements Schedulable{
    global Integer noOfDays;

    global skedAllRegionsNotificationSchedule() {

    }

    global skedAllRegionsNotificationSchedule(Integer noOfDays) {
        this.noOfDays = noOfDays;
    }

	global void execute(SchedulableContext sc) {
        try {
            DateTime currentTime = System.now();
            Map<String, String> map_regionId_scheduleString = new Map<String, String>();
            this.noOfDays = this.noOfDays != null ? this.noOfDays : 2;
            Decimal notifyTime = sked_Admin_Setting__c.getOrgDefaults().sked_2_Days_Notification_Email_Time__c != null ? sked_Admin_Setting__c.getOrgDefaults().sked_2_Days_Notification_Email_Time__c : 800;
            Integer notifyMinutes = skedDateTimeUtils.convertTimeNumberToMinutes(notifyTime);

            Map<String, sked__region__c> map_id_region = new Map<String, sked__region__c>();

            skedObjectSelector selector = skedObjectSelector.newInstance(sked__region__c.sObjectType);
            List<sked__region__c> skedRegions = Database.query(selector.getQuery());

            for (sked__region__c region : skedRegions) {
                if (String.isNotBlank(region.sked__Timezone__c)) {
                    DateTime regionTime = skedDateTimeUtils.getStartOfDate(Date.today(), region.sked__Timezone__c);
                    regionTime = skedDateTimeUtils.addMinutes(regionTime, notifyMinutes, region.sked__Timezone__c);

                    String scheduleTime = getSchedulingString(regionTime, 0);
                    map_regionId_scheduleString.put(region.id, scheduleTime);
                    map_id_region.put(region.id, region);
                }
            }

            if (!map_regionId_scheduleString.isEmpty()) {
                for (String regionId : map_regionId_scheduleString.keySet()) {
                    sked__region__c region = map_id_region.get(regionId);
                    String randomString = generateRandomString(6);
                    skedRegionSendNotificationSchedule notifySchedule = new skedRegionSendNotificationSchedule(regionId, region.sked__Timezone__c, this.noOfDays);
                    String notifyTimeString = map_regionId_scheduleString.get(regionId);
                    System.debug('notifyTimeString ' + notifyTimeString);
                    System.schedule('Send out Notification email in ' + region.name + ' region, token ' + randomString, notifyTimeString, notifySchedule);
                }
            }
        }
        catch (Exception ex) {
            skedCommonService.sendAdminNotificationEmail(new List<Exception>{ex});
        }

    }

    public static String generateRandomString(Integer len) {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < len) {
           Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
           randStr += chars.substring(idx, idx+1);
        }
        return randStr;
    }

    public static String getSchedulingString(DateTime refTime, Integer sec){
        dateTime dt= refTime.addSeconds(sec);
        String Csec, Cmin, Chr, Cday, Cmonth, CYear;
        Csec    = String.valueof(dt.second());
        Cmin    = String.valueof(dt.minute());
        Chr     = String.valueof(dt.hour());
        Cday    = String.valueof(dt.day());
        Cmonth  = String.valueof(dt.month());
        CYear   = String.valueof(dt.Year());

        // Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
        String SchTimer = Csec + ' ' + Cmin + ' ' + Chr + ' ' + Cday + ' ' + Cmonth + ' ? ' + CYear;
        return SchTimer;
    }
}
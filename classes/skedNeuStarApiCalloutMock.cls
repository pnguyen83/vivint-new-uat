@isTest
global class skedNeuStarApiCalloutMock implements WebServiceMock{
	global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
        skedNeuStarAPI.ElementResultType elementResult = new skedNeuStarAPI.ElementResultType();
        elementResult.id = 3226;
        elementResult.errorCode = 0;
        elementResult.value = '<Contact><Names><Name type="C"><First>Amanda</First><Last>Mitchell</Last></Name></Names><Addresses><Address validation="1" DPVConfirm="Y" USPSType="S" RBDI="B" score="14"><Street verified="200 PEARL ST">200 Pearl St</Street><City verified="NEW YORK">New York</City><ST verified="NY">NY</ST><Postal verified="10038-4915">10038</Postal></Address></Addresses><Phones><Phone validation="1" mobile="Y" active="1" score="96">8023100089</Phone></Phones><eMailAddresses><eMail validation="1" score="30">pnguyen@skedulo.com</eMail></eMailAddresses><IPAddresses /></Contact>';

        skedNeuStarAPI.ArrayOfElementResultType result = new skedNeuStarAPI.ArrayOfElementResultType();
        result.element = new List<skedNeuStarAPI.ElementResultType>{elementResult};

        skedNeuStarAPI.ResponseMsgType responseMsg = new skedNeuStarAPI.ResponseMsgType();
        responseMsg.transId = 1;
        responseMsg.errorCode = 0;
        responseMsg.errorValue = '';
        responseMsg.result = result;

        skedNeuStarAPI.queryResponse_element response_x = new skedNeuStarAPI.queryResponse_element();
        response_x.response = responseMsg;

        response.put('response_x', response_x);
   }
}
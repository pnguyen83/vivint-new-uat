@isTest
public class skedXmlUtils_Test {
    public static void createCustomSettings() {
        Skedulo_Neustar_Settings__c settings = new Skedulo_Neustar_Settings__c(
            sked_Username__c = 'test username',
            sked_Password__c = 'test password',
            sked_Service_ID__c = '123456'
        );
        insert settings;
    }

	@isTest static void testCreateContactXmlFile() {
        createCustomSettings();
        skedVivintModel.VivintContact contactData = new skedVivintModel.VivintContact();
        contactData.firstname = 'Amanda';
        contactData.lastname = 'Mitchell';
        contactData.email = 'pnguyen@skedulo.com';
        contactData.phone = '8023100089';
        contactData.street = '200 Pearl St';
        contactData.city = 'New York';
        contactData.state = 'NY';
        contactData.postalCode = '10038';
        test.startTest();
        String xmlString = skedXmlUtils.createContactXmlFile(contactData);
        System.assert(String.isNotBlank(xmlString));
        Test.setMock(WebServiceMock.class, new skedNeuStarApiCalloutMock());
        String xmlResponse = skedNeuStarApiUtils.callNeuStarApi(xmlString, 3226, 875);
        System.assert(String.isNotBlank(xmlResponse));
        DOM.XmlNode root = skedXmlUtils.getRootElementFromXmlString(xmlResponse);
        System.assert(root != null);
        List<skedVivintModel.ContactValidation> result = skedXmlUtils.getContactValidationResult(root);
        System.assert(!result.isEmpty());
        test.stopTest();
    }
}
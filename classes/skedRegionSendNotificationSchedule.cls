global class skedRegionSendNotificationSchedule implements Schedulable{
	global String regionId;
    global String timezone;
    global Integer noOfDay;

    global skedRegionSendNotificationSchedule(String regionId, String timezone, Integer noOfDay) {
		this.regionId = regionId;
        this.timezone = timezone;
        this.noOfDay = noOfDay;
	}

    global void execute(SchedulableContext sc) {
        if (String.isNotBlank(this.regionId)) {
            String regId = this.regionId;
            String job_status_pending_dispatch = skedConstants.JOB_STATUS_PENDING_DISPATCH;
            String job_status_pending_allocation = skedConstants.JOB_STATUS_PENDING_ALLOCATION;
            String email_template = skedConstants.NOTIFICATION_48H_EMAIL;
            List<Messaging.Singleemailmessage> emails = new List<Messaging.Singleemailmessage>();

            boolean enableOrgwide = sked_Admin_Setting__c.getOrgDefaults().sked_Enable_Org_Wide_Email__c;
            enableOrgwide = enableOrgwide == null ? false : enableOrgwide;

            Id orgWideEmailId = skedCommonService.getOrgWideEmail();

            String ccEmailAddress = sked_Admin_Setting__c.getOrgDefaults().sked_Notification_CC_Email_Address__c;
            ccEmailAddress = String.isNotBlank(ccEmailAddress) ? ccEmailAddress : skedConstants.ORG_WIDE_EMAIL_ADDRESS;

            Decimal notifyTime = sked_Admin_Setting__c.getOrgDefaults().sked_2_Days_Notification_Email_Time__c != null ? sked_Admin_Setting__c.getOrgDefaults().sked_2_Days_Notification_Email_Time__c : 800;
            Integer notifyMinutes = skedDateTimeUtils.convertTimeNumberToMinutes(notifyTime);

            DateTime selectedDate = System.now().addDays(this.noOfDay);

            Datetime startTime = skedDateTimeUtils.getStartOfDate(selectedDate, this.timezone);
            DateTime endTime = skedDateTimeUtils.getEndOfDate(selectedDate, this.timezone);
            String checkDay = selectedDate.format(skedConstants.YYYY_MM_DD, this.timezone);

            skedObjectSelector selector = skedObjectSelector.newInstance(EmailTemplate.sObjectType).filter('DeveloperName = :email_template');
            List<EmailTemplate> emailTemplates = Database.query(selector.getQuery());

            EmailTemplate template = emailTemplates.get(0);
            System.debug('startTime ' + startTime);
            System.debug('endTime ' + endTime);
            System.debug('checkDay ' + checkDay);
            skedObjectSelector selector2 = skedObjectSelector.newInstance(sked__Job__c.sObjectType);
            selector2.filter('sked__Region__c = :regId')
                     .filter('sked__Job_Status__c =: job_status_pending_dispatch OR sked__Job_Status__c =: job_status_pending_allocation');
            selector2.filter('sked__Start__c < :endTime').filter('sked__finish__c > : startTime');

            List<sked__Job__c> skedJobs = Database.query(selector2.getQuery());

            for (sked__Job__c skedJob : skedJobs) {
                System.debug('job ' + skedJob.name);
                String jobDay = skedJob.sked__Start__c.format(skedConstants.YYYY_MM_DD, this.timezone);

                if (checkDay.equalsIgnoreCase(jobDay)) {
                    String htmlBody = template.HtmlValue;
                    String customerName = skedJob.sked_Prospect_First_Name__c + ' ' + skedJob.sked_Prospect_Last_Name__c;
                    String confirmDate = skedJob.sked__Start__c.format(skedConstants.WEEKDAY_DAY_MONTH_YEAR, this.timezone);
                    String jobStart = skedJob.sked__Start__c.format(skedConstants.HOUR_MINUTES_ONLY, this.timezone);
                    String jobFinish = skedJob.sked__finish__c.format(skedConstants.HOUR_MINUTES_ONLY, this.timezone);
                    String arrivalWindow = jobStart + ' - ' + jobFinish;

                    htmlBody = htmlBody.replace('[SHP NAME]', customerName);
                    htmlBody = htmlBody.replace('[DATE]', confirmDate);
                    htmlBody = htmlBody.replace('[ARRIVAL TIME]', arrivalWindow);
                    htmlBody = htmlBody.replace('[ADDRESS]', skedJob.sked__Address__c);

                    String subject = template.Subject;

                    Messaging.Singleemailmessage email = new Messaging.Singleemailmessage();
                    email.setToAddresses(new List<String>{skedJob.sked_Customer_Email__c});
                    if (enableOrgwide) {
                        email.setOrgWideEmailAddressId(orgWideEmailId);
                    }
                    else {
                        email.setSenderDisplayName(skedConstants.DEFAULT_SENDER_NAME);
                    }

                    email.setSaveAsActivity(false);
                    email.setCcAddresses(new List<String>{ccEmailAddress});

                    email.setSubject(subject);
                    email.setHtmlBody(htmlBody);

                    emails.add(email);
                }
            }
            System.debug('emails ' + emails.size());
            if (!emails.isEmpty()) {
                List<Messaging.SendEmailResult> results = Messaging.sendEmail(emails);
                System.debug('results ' + results);
            }

            //abort schedule class after finish
            if (sc != null && sc.getTriggerId() != null) {
                System.abortJob(sc.getTriggerId());
            }

        }
    }
}
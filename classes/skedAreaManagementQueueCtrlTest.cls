@isTest
public with sharing class skedAreaManagementQueueCtrlTest {
	public static testmethod void getAllJobsTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        sked__region__c region = (sked__region__c)objFactory.getSObjects(sked__region__c.sObjectType).get(0);
        objFactory.createSlots(region.id);
        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        System.debug('job ' + job);
        skedVivintModel.AreaManagementFilter filter = new skedVivintModel.AreaManagementFilter();
        filter.regions = new List<String>{region.id};
        filter.getMyJobs = true;
        filter.displayUnallocatedJobs = true;
        filter.startDate = System.now().format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());
        filter.endDate = System.now().addDays(6).format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());
        filter.searchText = 'JOB';
        filter.status = new List<String>{'Pending Allocation', 'Allocated', 'Dispatched' , 'Ready', 'In Progress' , 'Complete', 'Cancelled', 'Pending Dispatch'};
        filter.closers = new List<String>();
        System.debug('filter ' + filter);
        test.startTest();
        skedRemoteResultModel result = skedAreaManagementQueueController.getAllJobs(filter);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void getUnallocatedJobsTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        sked__region__c region = (sked__region__c)objFactory.getSObjects(sked__region__c.sObjectType).get(0);
        objFactory.createSlots(region.id);
        Map<id, sked__Job__c> jobs = new Map<id, sked__Job__c>();
        List<String> dates = new List<String>();
        List<sked__Job__c> skedJobs = new List<sked__Job__c>();
        Integer index = 0;
        for (sked__Job__c skedJob : [SELECT id, sked__Start__c, sked__Timezone__c, (SELECT id FROM sked__Job_Allocations__r) FROM sked__Job__c]) {
            skedJob.sked__Region__c = region.id;
            dates.add(skedJob.sked__Start__c.format(skedConstants.YYYY_MM_DD, skedJob.sked__Timezone__c));
            if (!skedJob.sked__Job_Allocations__r.isEmpty() && index == 0) {
                index++;
                skedJobs.add(skedJob);
                continue;
            }
            jobs.put(skedJob.id, skedJob);

        }
        skedJobs.addAll(jobs.values());
        update skedJobs;

        List<sked__Job_Allocation__c> skedJAs = [SELECT id FROM sked__Job_Allocation__c WHERE sked__Job__c IN : jobs.keySet()];
        delete skedJAs;


        skedVivintModel.AreaManagementFilter filter = new skedVivintModel.AreaManagementFilter();
        filter.regions = new List<String>{region.id};
        filter.getMyJobs = true;
        filter.displayUnallocatedJobs = true;
        filter.startDate = System.now().format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());
        filter.endDate = System.now().addDays(6).format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());
        filter.searchText = 'JOB';
        filter.dayList = new List<String>(dates);
        filter.status = new List<String>{'Pending Allocation', 'Allocated', 'Dispatched' , 'Ready', 'In Progress' , 'Complete', 'Cancelled', 'Pending Dispatch'};
        filter.closers = new List<String>{UserInfo.getUserId()};
        System.debug('filter ' + filter);
        test.startTest();
        skedRemoteResultModel result = skedAreaManagementQueueController.getUnAllocatedJobs(filter);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void getUnallocatedJobsTest2() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        sked__region__c region = (sked__region__c)objFactory.getSObjects(sked__region__c.sObjectType).get(0);
        objFactory.createSlots(region.id);
        Map<id, sked__Job__c> jobs = new Map<id, sked__Job__c>();
        List<String> dates = new List<String>();

        for (sked__Job__c skedJob : [SELECT id, sked__Start__c, sked__Timezone__c, (SELECT id FROM sked__Job_Allocations__r) FROM sked__Job__c]) {
            skedJob.sked__Region__c = region.id;
            jobs.put(skedJob.id, skedJob);
            dates.add(skedJob.sked__Start__c.format(skedConstants.YYYY_MM_DD, skedJob.sked__Timezone__c));
        }

        update jobs.values();

        List<sked__Job_Allocation__c> skedJAs = [SELECT id FROM sked__Job_Allocation__c WHERE sked__Job__c IN : jobs.keySet()];
        delete skedJAs;


        skedVivintModel.AreaManagementFilter filter = new skedVivintModel.AreaManagementFilter();
        filter.regions = new List<String>{region.id};
        filter.getMyJobs = true;
        filter.displayUnallocatedJobs = true;
        filter.startDate = System.now().format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());
        filter.endDate = System.now().addDays(6).format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());
        filter.searchText = 'JOB';
        filter.dayList = new List<String>(dates);
        filter.status = new List<String>{'Pending Allocation', 'Allocated', 'Dispatched' , 'Ready', 'In Progress' , 'Complete', 'Cancelled', 'Pending Dispatch'};
        filter.closers = new List<String>{UserInfo.getUserId()};
        System.debug('filter ' + filter);

        test.startTest();
        skedRemoteResultModel result = skedAreaManagementQueueController.getUnAllocatedJobs(filter);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void getAllJobsTest2() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        sked__region__c region = (sked__region__c)objFactory.getSObjects(sked__region__c.sObjectType).get(0);
        objFactory.createSlots(region.id);
        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        System.debug('job ' + job);
        skedVivintModel.AreaManagementFilter filter = new skedVivintModel.AreaManagementFilter();
        filter.regions = new List<String>{region.id};
        filter.getMyJobs = true;
        filter.displayUnallocatedJobs = true;
        filter.startDate = System.now().format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());
        filter.endDate = System.now().addDays(6).format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());

        filter.status = new List<String>{'Pending Allocation', 'Allocated', 'Dispatched' , 'Ready', 'In Progress' , 'Complete', 'Cancelled', 'Pending Dispatch'};

        System.debug('filter ' + filter);
        test.startTest();
        skedRemoteResultModel result = skedAreaManagementQueueController.getAllJobs(filter);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void getConfigDataTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        skedRemoteResultModel result = skedAreaManagementQueueController.getConfigData();
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void getConfigDataTest2() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        sked_Queue_Management_Access__c regionManager = (sked_Queue_Management_Access__c)objFactory.getSObjects(sked_Queue_Management_Access__c.sObjectType).get(0);
        delete regionManager;

        test.startTest();
        skedRemoteResultModel result = skedAreaManagementQueueController.getConfigData();
        System.debug('result ' + result);
        System.assert(result.success == false);
        test.stopTest();
    }

    public static testmethod void cancelJobTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        skedVivintModel.VivintJob data = new skedVivintModel.VivintJob();
        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        data.Id = job.id;
        data.reason = 'Canceled: On-Site';

        test.startTest();
        skedRemoteResultModel result = skedAreaManagementQueueController.cancelJob(data);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void reassignJobTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        skedVivintModel.JobAvaiModel filter = new skedVivintModel.JobAvaiModel();
        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        sked__Resource__c res1 = (sked__Resource__c)objFactory.getSObjects(sked__Resource__c.sObjectType).get(0);
        sked__Resource__c res2 = res1.clone(false,false,false, false);
        res2.sked__User__c = null;
        res2.name = 'test resource 2';
        res2.Vivint_Employee_Id__c = 222;
        System.debug('res1 ' + res1);
        System.debug('res2 ' + res2);
        insert res2;
        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        objFactory.createSlots(region.id);
        filter.jobId = job.id;
        filter.regions = new List<String>{region.id};
        filter.selectedDate = System.now().format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());

        test.startTest();
        skedRemoteResultModel result = skedAreaManagementQueueController.reassignJob(filter);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void confirmAssignmentTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        User u = skedSetterControllerTest.createUser();
        skedVivintModel.Assignment data = new skedVivintModel.Assignment();
        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        sked__Resource__c res = (sked__Resource__c)objFactory.getSObjects(sked__Resource__c.sObjectType).get(0);
        sked__Resource__c resource2 = res.clone(false, false, false, false);
        resource2.sked__UniqueKey__c = 'mail@mail.com';
        resource2.name = 'res 2';
        resource2.sked__User__c = u.id;
        resource2.Vivint_Employee_Id__c = 1238;
        insert resource2;

        skedSObjectFactoryExt ext = new skedSObjectFactoryExt();
        ext.createSlots(job.sked__Region__c);
        List<sked__Slot__c> slots = [SELECT id FROM sked__Slot__c LIMIT 1];

        data.jobId = job.id;
        data.resourceIDs = new List<String>{res.id, resource2.id};
        data.slotId = slots.get(0).id;
        test.startTest();
        skedRemoteResultModel result = skedAreaManagementQueueController.confirmAssignment(data);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void confirmAssignmentTest2() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        User u = skedSetterControllerTest.createUser();
        skedVivintModel.Assignment data = new skedVivintModel.Assignment();
        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        sked__Resource__c res = (sked__Resource__c)objFactory.getSObjects(sked__Resource__c.sObjectType).get(0);
        sked__Resource__c resource2 = res.clone(false, false, false, false);
        resource2.sked__UniqueKey__c = 'mail@mail.com';
        resource2.name = 'res 2';
        resource2.sked__User__c = u.id;
        resource2.Vivint_Employee_Id__c = 1238;
        insert resource2;

        data.jobId = job.id;
        data.resourceIDs = new List<String>{resource2.id};

        test.startTest();
        skedRemoteResultModel result = skedAreaManagementQueueController.confirmAssignment(data);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void getAllResourcesTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        skedVivintModel.AreaManagementFilter filter = new skedVivintModel.AreaManagementFilter();
        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        filter.regions = new List<String>{region.id};
        objFactory.createSlots(region.id);
        test.startTest();
        skedRemoteResultModel result = skedAreaManagementQueueController.getAllResources(filter);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void getResourceAndJobsInRegionsTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        skedVivintModel.AreaManagementFilter filter = new skedVivintModel.AreaManagementFilter();
        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        objFactory.createSlots(region.id);
        filter.regions = new List<String>{region.id};
        filter.displayUnallocatedJobs = true;
        filter.resourceList = new List<skedModels.resource>();
        String day1 = System.now().format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());
        String day2 = System.now().addDays(1).format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());
        String day3 = System.now().addDays(2).format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());
        filter.dayList = new List<String>{day1, day2, day3};
        sked__Resource__c resource = (sked__Resource__c)objFactory.getSObjects(sked__Resource__c.sObjectType).get(0);
        skedModels.resource res = new skedModels.resource(resource);
        filter.resourceList.add(res);

        test.startTest();
        skedRemoteResultModel result = skedAreaManagementQueueController.getResourceAndJobsInRegions(filter);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void getResourcesForRescheduleTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();


        skedVivintModel.AreaManagementFilter filter = new skedVivintModel.AreaManagementFilter();
        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        objFactory.createSlots(region.id);
        filter.regions = new List<String>{region.id};
        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        sked__Job_Allocation__c ja = (sked__Job_Allocation__c)objFactory.getSObjects(sked__Job_Allocation__c.sObjectType).get(0);
        String timezone = UserInfo.getTimeZone().getId();

        filter.selectedJob = new skedVivintModel.VivintJA(job, ja);
        String day1 = System.now().format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());
        filter.dayList = new List<String>{day1};

        test.startTest();
        skedRemoteResultModel result = skedAreaManagementQueueController.getResourcesForReschedule(filter);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void rescheduleJobTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        skedVivintModel.RescheduleJob data = new skedVivintModel.RescheduleJob();
        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        objFactory.createSlots(job.sked__Region__c);

        data.jobId = job.id;
        data.selectedDate = System.now().format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());
        data.startTime = 800;
        data.endTime = 1100;
        String weekDay = System.now().format(skedConstants.WEEKDAY, UserInfo.getTimeZone().getId());

        List<sked__Slot__c> slots = [SELECT Id FROM sked__Slot__c WHERE sked_Day__c =:weekDay];
        sked__Slot__c slot = slots.get(0);
        slot.sked_Booking_Type__c = skedConstants.SLOT_UNALLOCATED_TYPE;
        job.sked__job_status__c = 'ready';
        job.sked_Slot__c = slot.id;
        List<sObject> objects = new List<sObject>{slot, job};
        update objects;
        if (!slots.isEmpty()) {
            data.slotId = slot.id;
        }

        test.startTest();
        skedRemoteResultModel result = skedAreaManagementQueueController.rescheduleJob(data);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void rescheduleJobTest2() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        skedVivintModel.RescheduleJob data = new skedVivintModel.RescheduleJob();
        sked__Job__c originateJob = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        String jobId = originateJob.id;
        String completedJA = skedConstants.JOB_ALLOCATION_STATUS_COMPLETE;
        String declinedJA = skedConstants.JOB_ALLOCATION_STATUS_DECLINED;
        String deletedJA = skedConstants.JOB_ALLOCATION_STATUS_DELETED;
        skedObjectSelector selector2 = skedObjectSelector.newInstance(sked__Job__c.sObjectType);
        selector2.filter('id = :jobId');
        selector2.subQuery('sked__Job_Allocations__r').filter('sked__Status__c !=:completedJA')
                                                        .filter('sked__Status__c !=:declinedJA')
                                                        .filter('sked__Status__c != :deletedJA');
        List<sked__Job__c> jobs = Database.query(selector2.getQuery());

        if (!jobs.isEmpty()) {
            sked__Job__c job = jobs.get(0);
            if (!job.sked__Job_Allocations__r.isEmpty()) {
                delete job.sked__Job_Allocations__r;
            }
            sked__Resource__c res = (sked__Resource__c)objFactory.getSObjects(sked__Resource__c.sObjectType).get(0);
            objFactory.createSlots(job.sked__Region__c);

            data.jobId = job.id;
            data.selectedDate = System.now().format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());
            data.startTime = 800;
            data.endTime = 1100;
            data.resourceId = res.id;
            String weekDay = System.now().format(skedConstants.WEEKDAY, UserInfo.getTimeZone().getId());

            List<sked__Slot__c> slots = [SELECT Id FROM sked__Slot__c WHERE sked_Day__c =:weekDay];
            sked__Slot__c slot = slots.get(0);
            slot.sked_Booking_Type__c = skedConstants.SLOT_UNALLOCATED_TYPE;
            job.sked__job_status__c = 'ready';
            job.sked_Slot__c = slot.id;
            List<sObject> objects = new List<sObject>{slot, job};
            update objects;
            if (!slots.isEmpty()) {
                data.slotId = slot.id;
            }
            test.startTest();
            skedRemoteResultModel result = skedAreaManagementQueueController.rescheduleJob(data);
            System.debug('result ' + result);
            System.assert(result.success == true);
            test.stopTest();
        }

    }

    public static testmethod void confirmPendingDispatchJobTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();


        skedVivintModel.Appointment data = new skedVivintModel.Appointment();
        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        data.jobId = job.id;
        test.startTest();
        skedRemoteResultModel result = skedAreaManagementQueueController.confirmPendingDispatchJob(data);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void testUpdateAttemptedJob() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();


        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        skedVivintModel.VivintJob jobData = new skedVivintModel.VivintJob();
        jobData.id = job.id;
        test.startTest();
        skedRemoteResultModel result = skedAreaManagementQueueController.updateAttemptedJob(jobData);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void testUpdateJobInfo() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        skedVivintModel.VivintContact data = new skedVivintModel.VivintContact(job);
        test.startTest();
        skedRemoteResultModel result = skedAreaManagementQueueController.updateJobInfo(data);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void testCreateJobProduct() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        skedModels.selectOption option = new skedModels.selectOption('Google Mesh WiFi', 'Google Mesh WiFi');
        option.quantity = 2;
        List<skedModels.selectOption> accessoryTypes = new List<skedModels.selectOption>{option};

        test.startTest();
        List<sked__Job_Product__c> result = skedCommonservice.createJobProducts(job.id, accessoryTypes);
        System.debug('result ' + result);
        System.assert(!result.isEmpty());
        test.stopTest();
    }

    public static testmethod void filterSlotAndJobByDayPeriodHaveDateTest () {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        objFactory.createSlots(job.sked__Region__c);
        skedVivintModel.VivintJA ja = new skedVivintModel.VivintJA(job, null);
        List<skedVivintModel.VivintJA> jas = new List<skedVivintModel.VivintJA>{ja};
        skedVivintModel.AreaManagementFilter filter = new skedVivintModel.AreaManagementFilter();
        filter.startDate = System.now().addDays(-1).format(skedConstants.YYYY_MM_DD);
        filter.endDate = System.now().addDays(1).format(skedConstants.YYYY_MM_DD);
        filter.dayPeriods = new List<String>{'morning'};
        filter.regions = new List<String>{job.sked__Region__c};
        test.startTest();
        List<skedVivintModel.TimeSlot> result = skedCommonservice.filterSlotAndJobByDayPeriod(jas, filter);
        System.debug('result ' + result);
        System.assert(!result.isEmpty());
        test.stopTest();
    }

    public static testmethod void filterSlotAndJobByDayPeriodNoDateTest () {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        objFactory.createSlots(job.sked__Region__c);
        skedVivintModel.VivintJA ja = new skedVivintModel.VivintJA(job, null);
        List<skedVivintModel.VivintJA> jas = new List<skedVivintModel.VivintJA>{ja};
        skedVivintModel.AreaManagementFilter filter = new skedVivintModel.AreaManagementFilter();
        filter.dayPeriods = new List<String>{'morning'};
        filter.regions = new List<String>{job.sked__Region__c};
        test.startTest();
        List<skedVivintModel.TimeSlot> result = skedCommonservice.filterSlotAndJobByDayPeriod(jas, filter);
        System.debug('result ' + result);
        System.assert(!result.isEmpty());
        test.stopTest();
    }
}
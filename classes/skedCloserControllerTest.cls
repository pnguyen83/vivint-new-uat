@isTest
public with sharing class skedCloserControllerTest {
	public static testmethod void getConfigDataTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        skedRemoteResultModel result = skedCloserController.getConfigData();
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void searchLocationTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked_Setter_Location__c location = (sked_Setter_Location__c)objFactory.getSObjects(sked_Setter_Location__c.sObjectType).get(0);
        skedRemoteResultModel result = skedCloserController.searchLocation(location.name);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void getJobsTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();
        String timezone = UserInfo.getTimeZone().getId();
        test.startTest();
        skedVivintModel.CloserData data = new skedVivintModel.CloserData();
        data.startDate = System.now().format(skedConstants.YYYY_MM_DD, timezone);
        data.endDate = System.now().format(skedConstants.YYYY_MM_DD, timezone);
        sked__Resource__c resource = (sked__Resource__c)objFactory.getSObjects(sked__Resource__c.sObjectType).get(0);
        resource.sked__User__c = UserInfo.getUserId();
        update resource;

        skedRemoteResultModel result = skedCloserController.getJobs(data);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void getJobsTest2() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();
        String timezone = UserInfo.getTimeZone().getId();
        test.startTest();
        skedVivintModel.CloserData data = new skedVivintModel.CloserData();
        data.startDate = System.now().format(skedConstants.YYYY_MM_DD, timezone);
        data.endDate = System.now().format(skedConstants.YYYY_MM_DD, timezone);

        skedRemoteResultModel result = skedCloserController.getJobs(data);
        System.assert(result.success == true);
        System.debug('result ' + result);
        test.stopTest();
    }

    public static testmethod void getRescheduleAvailabilityTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        String timezone = UserInfo.getTimeZone().getId();
        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        objFactory.createSlots(region.id);
        sked__job__c job = (sked__job__c)objFactory.getSObjects(sked__job__c.sObjectType).get(0);
        skedVivintModel.JobAvaiModel data = new skedVivintModel.JobAvaiModel();
        data.regions = new List<String>{region.id};
        data.jobId = job.id;
        data.selectedDate = System.now().format(skedConstants.YYYY_MM_DD, timezone);

        skedRemoteResultModel result = skedCloserController.getRescheduleAvailability(data);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void getRescheduleAvailabilityTest2() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        String timezone = UserInfo.getTimeZone().getId();
        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        objFactory.createSlots(region.id);
        sked__job__c job = (sked__job__c)objFactory.getSObjects(sked__job__c.sObjectType).get(0);
        skedVivintModel.JobAvaiModel data = new skedVivintModel.JobAvaiModel();
        data.regions = new List<String>{region.id};
        data.jobId = job.id;

        skedRemoteResultModel result = skedCloserController.getRescheduleAvailability(data);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void rescheduleJobTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        skedVivintModel.RescheduleJob data = new skedVivintModel.RescheduleJob();
        sked__job__c job = (sked__job__c)objFactory.getSObjects(sked__job__c.sObjectType).get(0);
        data.jobId = job.id;
        data.selectedDate = System.now().format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());
        data.startTime = 800;
        data.endTime = 1100;
        sked__Resource__c resource = (sked__Resource__c)objFactory.getSObjects(sked__Resource__c.sObjectType).get(0);
        resource.sked__User__c = UserInfo.getUserId();
        update resource;

        skedRemoteResultModel result = skedCloserController.rescheduleJob(data);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void confirmJobTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        skedVivintModel.Appointment data = new skedVivintModel.Appointment();
        sked__job__c job = (sked__job__c)objFactory.getSObjects(sked__job__c.sObjectType).get(0);
        data.jobId = job.id;

        skedRemoteResultModel result = skedCloserController.confirmJob(data);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void cancelJobTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        skedVivintModel.Appointment data = new skedVivintModel.Appointment();
        sked__job__c job = (sked__job__c)objFactory.getSObjects(sked__job__c.sObjectType).get(0);
        data.jobId = job.id;
        data.isCancel = true;
        data.reason = 'Canceled: On-Site';

        skedRemoteResultModel result = skedCloserController.cancelJob(data);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void getMoreJobInfoTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked__job__c job = (sked__job__c)objFactory.getSObjects(sked__job__c.sObjectType).get(0);

        skedRemoteResultModel result = skedCloserController.getMoreJobInfo(job.id);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void updateJobInfoTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked__job__c job = (sked__job__c)objFactory.getSObjects(sked__job__c.sObjectType).get(0);
        skedVivintModel.VivintContact contact = new skedVivintModel.VivintContact(job);

        sked_Setter_Location__c location = (sked_Setter_Location__c)objFactory.getSObjects(sked_Setter_Location__c.sObjectType).get(0);
        job.sked_Setter_Location__c = location.id;
        update job;
        contact.jobId = job.id;

        skedRemoteResultModel result = skedCloserController.updateJobInfo(contact);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void searchJobTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked__region__c region = (sked__region__c)objFactory.getSObjects(sked__region__c.sObjectType).get(0);
        objFactory.createSlots(region.id);
        skedVivintModel.AreaManagementFilter filter = new skedVivintModel.AreaManagementFilter();
        filter.regions = new List<String>{region.id};
        filter.getMyJobs = true;
        filter.startDate = System.now().format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());
        filter.endDate = System.now().addDays(6).format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());

        filter.status = new List<String>{'Pending Allocation', 'Allocated', 'Dispatched' , 'Ready', 'In Progress' , 'Complete', 'Cancelled', 'Pending Dispatch'};

        skedRemoteResultModel result = skedCloserController.searchJob(filter);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void updateAttemptedJobTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked__region__c region = (sked__region__c)objFactory.getSObjects(sked__region__c.sObjectType).get(0);
        objFactory.createSlots(region.id);
        sked__job__c skedJob = (sked__job__c)objFactory.getSObjects(sked__job__c.sObjectType).get(0);

        skedVivintModel.VivintJob job = new skedVivintModel.VivintJob();
        job.id = skedJob.id;
        job.jobStatus = skedJob.sked__Job_Status__c;


        skedRemoteResultModel result = skedCloserController.updateAttemptedJob(job);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void getTimeLabelOfDayTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked__region__c region = (sked__region__c)objFactory.getSObjects(sked__region__c.sObjectType).get(0);
        objFactory.createSlots(region.id);

        skedVivintModel.TimeSlotFilterModel filter = new skedVivintModel.TimeSlotFilterModel();
        filter.onlySelectedDate = true;
        filter.selectedDate = System.now().format(skedConstants.YYYY_MM_DD);


        skedRemoteResultModel result = skedCloserController.getTimeLabelOfDay(filter);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void  getBookingGridTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        Test.startTest();
        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        objFactory.createSlots(region.id);
        skedVivintModel.CloserFilterData data = new skedVivintModel.CloserFilterData();
        data.startDate = System.now().format(skedConstants.YYYY_MM_DD);
        data.endDate = System.now().addDays(20).format(skedConstants.YYYY_MM_DD);
        data.jobLocation = new skedModels.geometry(40.7376731, -73.9897125);
        data.postalCode = '00002';
        skedRemoteResultModel result = skedCloserJobCreationCtrl.getBookingGrid(data);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void searchLocationTest2() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked_Setter_Location__c location = (sked_Setter_Location__c)objFactory.getSObjects(sked_Setter_Location__c.sObjectType).get(0);
        skedRemoteResultModel result = skedCloserJobCreationCtrl.searchLocation(location.Name);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }



    public static testmethod void  saveJobTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        Test.startTest();
        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        objFactory.createSlots(region.id);
        sked__Resource__c resource = (sked__Resource__c)objFactory.getSObjects(sked__Resource__c.sObjectType).get(0);
        String timezone = region.sked__Timezone__c;
        objFactory.createSlots(region.id);

        List<sked__Slot__c> slots = [SELECT id FROM sked__Slot__c ];

        skedAvailatorParams params = new skedAvailatorParams();
        params.timezoneSidId = region.sked__Timezone__c;
        params.startDate = skedDateTimeUtils.getDate(System.now(), timezone);
        params.endDate = skedDateTimeUtils.getDate(System.now(), timezone);
        params.resourceIds = new Set<Id>{resource.id};
        params.inputDates = new Set<Date>{System.today(), System.today().addDays(1)};
        params.regionId = region.Id;

        skedResourceAvailability resourceAvailability = new skedResourceAvailability(params);
        Map<String, skedResourceAvailabilityBase.resourceModel> mapResource = resourceAvailability.initializeResourceList();

        skedVivintModel.Appointment appointment = new skedVivintModel.Appointment();
        appointment.contactInfo = new skedVivintModel.VivintContact();
        sked__Region_Area__c regionArea = (sked__Region_Area__c)objFactory.getSObjects(sked__Region_Area__c.sObjectType).get(0);
        sked_Setter_Location__c location = (sked_Setter_Location__c)objFactory.getSObjects(sked_Setter_Location__c.sObjectType).get(0);
        appointment.contactInfo.postalCode = regionArea.sked_Zip_Code_Name__c;
        appointment.contactInfo.location = new skedModels.skedLocation(location);
        appointment.contactInfo.firstname = 'contact first name1';
        appointment.contactInfo.lastname = 'last name';
        appointment.contactInfo.areaInterestedIn = new List<String>{'Smart Assistant'};
        appointment.contactInfo.email = 'test@email.com';
        appointment.contactInfo.haveHighSpeedInternet = 'Yes';
        appointment.contactInfo.howInterestedInSH = '10';
        appointment.contactInfo.ownHomeOrBusiness = 'Yes';
        appointment.contactInfo.phone = '10';
        appointment.contactInfo.repId = '10';
        appointment.contactInfo.haveSecuritySystem = 'Yes';
        appointment.contactInfo.timeHaveSystem = '10';
        appointment.contactInfo.systemMonitored = 'Yes';
        appointment.contactInfo.setterName = 'Setter';
        appointment.contactInfo.decisionMaking = 'Yes';
        appointment.contactInfo.accountNumber = '10';

        appointment.jobInfo.jobDate = System.now().format(skedConstants.YYYY_MM_DD, timezone);
        appointment.jobInfo.jobStart = 1200;
        appointment.jobInfo.jobFinish = 1500;
        appointment.jobInfo.address = '228 Park Ave S, New York';
        appointment.jobInfo.slotId = slots.get(0).id;

        for (skedResourceAvailabilityBase.resourceModel res : mapResource.values()) {
            if (appointment.lstRes == null) {
                appointment.lstRes = new List<skedResourceAvailabilityBase.resourceModel>();
            }
            appointment.lstRes.add(res);
        }
        appointment.contactInfo.location.geoLocation = new skedModels.geometry(location.sked_GeoLocation__Latitude__s, location.sked_GeoLocation__Longitude__s);
        skedRemoteResultModel result = skedCloserJobCreationCtrl.saveJob(appointment);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }
}
public class skedResourceHandler {
	public static void onAfterInsert(List<sked__Resource__c> lstNews) {
		udpateNumberOfCloserOnRegion(lstNews, null);
	}

	public static void onAfterUpdate(List<sked__Resource__c> lstNews, Map<id, sked__Resource__c> mapOlds) {
		udpateNumberOfCloserOnRegion(lstNews, mapOlds);
	}

	public static void onAfterDelete(List<sked__Resource__c> lstOlds) {
		udpateNumberOfCloserOnRegion(lstOlds, null);
	}

	//********************************************Private Functions**********************************************//
	private static void udpateNumberOfCloserOnRegion(List<sked__Resource__c> lstNews, Map<id, sked__Resource__c> mapOlds) {
		Set<String> setRegionIDs = new Set<String>();

		for (sked__Resource__c res : lstNews) {
			if (res.sked__Primary_Region__c != null) {
				if (mapOlds != null) {
					if (mapOlds.containsKey(res.id)) {
						sked__Resource__c oldres = mapOlds.get(res.id);
						if (oldres.sked__Primary_Region__c == null || oldres.sked__Primary_Region__c != res.sked__Primary_Region__c ||
							oldRes.sked__is_Active__c != res.sked__Is_Active__c) {
							setRegionIDs.add(res.sked__Primary_Region__c);
							setRegionIDs.add(oldres.sked__Primary_Region__c);
						}
					}
				}
				else {
					setRegionIDs.add(res.sked__Primary_Region__c);
				}
			}
		}

		if (!setRegionIDs.isEmpty()) {
			List<sked__Region__c> lstRegions = new List<sked__Region__c>();

			for (sked__Region__c reg : [SELECT Id, (SELECT Id FROM sked__Resources__r WHERE sked__Is_Active__c = true)
											FROM sked__Region__c
											WHERE Id IN : setRegionIDs]) {
				sked__Region__c region = new sked__Region__c(
					id = reg.Id,
					sked_Number_of_Closers__c = reg.sked__Resources__r.size()
				);
				lstRegions.add(region);
			}

			if (!lstRegions.isEmpty()) {
				update lstRegions;
			}
		}
	}
}
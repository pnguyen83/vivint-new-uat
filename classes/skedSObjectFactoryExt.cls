public class skedSObjectFactoryExt extends skedSObjectFactory{

    /**
    * @description override to initialize more objects
    * @param obj
    */
    public override void initMore(){
        this.getObject(sked__Resource__c.sObjectType).fields(new Map<String, Object>{
                'sked__GeoLocation__latitude__s'     => 40.7376731,
                'sked__GeoLocation__longitude__s'    => -73.9897125,
                'sked__Resource_Type__c'    => 'Person',
                'sked__User__c' => UserInfo.getUserId(),
                'sked__Mobile_Phone__c' => '12345678'
                });
        this.getObject(sked__job__c.sObjectType).fields(new Map<String, Object>{
                'sked_Customer_Email__c'     => 'abc@email.com',
                'sked__Contact__c' => this.newRelationship(Contact.sObjectType),
                'sked_accepts_Vivint_Policy__c' => true,
                'ssked_Prospect_Additional_information__c' => 'abc',
                'sked_Prospect_Address__c' => '123 abc',
                'sked_Prospect_has_speed_internet__c' => 'yes',
                'sked_Prospect_owns_Home_or_Business__c' => 'yes',
                'sked_Prospect_interested_in_Smart_Home__c' => '10',
                'sked_Prospect_Rep_ID__c' => 'abc',
                'sked_Prospect_Name__c' => 'sked_Prospect_Name__c',
                'sked_Vivint_Account_Number__c' => 'abc',
                'sked_Area_Interested_In_Smart_Home__c' => 'Smart Assistant',
                'sked_Customer_Phone__c' => '123456',
                'sked_Setter_Name__c' => 'abc',
                'sked_Will_a_decision_maker_be_present__c' => 'yes',
                'sked_Do_you_have_security_system__c' => 'yes',
                'sked_How_long_have_you_had_your_system__c' => '12',
                'sked_Is_your_system_being_monitored__c' => 'yes'
                });
        this.newObject(sked__Region_Area__c.sObjectType).fields(new Map<String,Object>{
                'sked_Zip_Code_Name__c'                 => '00002',
                'sked__Region__c'           => this.newRelationship(sked__Region__c.sObjectType)
            });

        this.newObject(sked__Holiday__c.sObjectType).fields(new Map<String,Object>{
                'Name' => 'Holiday 1',
                'sked__Start_Date__c' => System.today(),
                'sked__End_Date__c' => System.today().addDays(2),
                'sked__Global__c' => true
            });

        this.newObject(sked_Setter_Location__c.sObjectType).fields(new Map<String,Object>{
                'Name'                 => 'Loc 00001',
                'sked_Region__c '           => this.newRelationship(sked__Region__c.sObjectType),
                'sked_GeoLocation__latitude__s'     => 40.7376731,
                'sked_GeoLocation__longitude__s'    => -73.9897125
            });

        this.newObject(sked_Setter__c.sObjectType).fields(new Map<String,Object>{
                'Name'                 => 'Setter 1',
                'sked_Access_ID__c '           => '1234',
                'sked_Active__c '     => true
            });

        this.newObject(sked_Queue_Management_Access__c.sObjectType).fields(new Map<String,Object>{
                'sked_Region__c'           => this.newRelationship(sked__Region__c.sObjectType),
                'sked_Resource__c'           => this.newRelationship(sked__Resource__c.sObjectType),
                'sked_Role__c'     => 'Regional Manager',
                'sked_Primary__c'  => true
            });

        this.newObject(Skedulo_SMS__c.sObjectType).fields(new Map<String,Object>{
                'api_key__c'           => '357e2907',
                'api_secret__c'           => '6e7bd178',
                'sked_Auto_Allocation_Notification_SMS__c'      => 'Alert: [QUANTITY] confirmed jobs could not be assigned for the [SLOT_NAME] time slot. Please go to the Skedulo queue to manage these.',
                'sked_Confirm_Job_Message__c' => 'You have a new confirmed appointment! [JOBNAME] for [CONTACTNAME] on [JOBTIME] at [HOUR]',
                'Country_Code__c' => 'US',
                'Default_SMS__c' => 'abc',
                'EnrouteSMS__c' => 'Hi [CONTACTNAME], Your Technician is on their way to you and is scheduled to arrive at [JOBSTARTTIME]',
                'From_Phone_Number__c' => '12345678',
                'PendingDispatchSMS__c' => 'You have a new appointment! [JOBNAME] for [CONTACTNAME] on [JOBTIME] at [HOUR]',
                'sked_Unallocated_Job_Message__c' => 'You have a new unallocated appointment! [JOB_NUMBER] for [CUSTOMER] on [JOB_DATE] at [JOB_TIME]'
            });

        this.newObject(sked_Admin_Setting__c.sObjectType).fields(new Map<String,Object>{
                'Velocity__c'                 => 50,
                'Skedulo_API_Token__c'           => 'abcdfrdfgdfgdfg',
                'sked_Ignore_Travel_Time_First_Job_Of_Day__c' => true,
                'sked_Enable_Confirmation_Email__c' => true,
                'sked_Enable_Rescheduled_Email__c' => true,
                'sked_Enable_1_day_notification_email__c' => true,
                'sked_Availability_Notification_Template__c' => 'test template',
                'sked_Administrator_Email__c' => 'abc@xyz.com',
                'sked_Enable_Org_Wide_Email__c' => false,
                'sked_Interval_Time__c' => 10,
                'sked_Notification_CC_Email_Address__c' => 'qwe@asd.zcc',
                'sked_Org_wide_Email_Address__c' => 'in-homeconsultation@vivint.com',
                'sked_Public_Site_URL__c' => 'http://vivintuat-vivint-uat.cs65.force.com'
            });


        this.newObject(sked_Setter_Interface_Version__c.sObjectType).fields(new Map<String, Object>{
                'Name' => 'ptest1',
                'sked_Unique_Partner_ID__c' => 'abc123',
                'sked_Appointment_Status__c' => 'Confirmed',
                'sked_Appointment_Type__c' => 'In-Home Consultation',
                'sked_BCC_Email_Addresses__c' => 'abc@test.mail',
                'sked_Is_Address__c' => true,
                'sked_Is_Interest_Area__c' => true,
                'sked_Is_Decision_Maker__c' => true,
                'sked_Is_Email__c' => true,
                'sked_Is_First_Name__c' => true,
                'sked_Is_Has_Securit_System__c' => true,
                'sked_Is_High_Speed_Internet__c' => true,
                'sked_Is_Interested_Point__c' => true,
                'sked_Is_System_Monitored__c' => true,
                'sked_Is_Last_Name__c' => true,
                'sked_Is_Location__c' => true,
                'sked_Is_Note__c' => true,
                'sked_Is_Own_Home__c' => true,
                'sked_Is_Phone__c' => true,
                'sked_Is_Setter_Id__c' => true,
                'sked_Is_Setter_Name__c' => true,
                'sked_Is_Time_Has_Security_System__c' => true,
                'sked_Is_Vivint_Account_Number__c' => true,
                'sked_Partner_Type__c' => 'NIS',
                'sked_Force_Partner_ID__c' => 'No'
            });
        this.newObject(sked_Partner__c.sObjectType).fields(new Map<String, Object>{
                'Name' => 'ptest1',
                'sked_Partner_Type__c' => 'NIS',
                'sked_Partner_Unique_ID__c' => '123'
            });
    }

    /**
    * @description override to initialize an instance of SObjectModelExt
    *
    */
    public override SObjectModel initNewObject(Schema.sObjectType sObjectType){
        return new SObjectModelExt(sObjectType);
    }

    /**
    * @description extend SObjectModel format more objects
    */
    public  class SObjectModelExt extends SObjectModel{

        public SObjectModelExt(Schema.sObjectType sObjectType){
            super(sObjectType);
        }

        /**
        * @description This method will be overridden by a subclass to format data before creation
        * @param sObjectType
        * @param obj
        */
        public override void formatMore(Schema.sObjectType sObjectType, List<sObject> objects){

        }
    }

    public void createSlots(String regionId) {
        List<sked__Slot__c> slots = new List<sked__Slot__c>();
        List<String> weekdays = new List<String>{'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'};

        for (Integer i = 0; i < 7; i++) {
            sked__Slot__c slot = new sked__Slot__c(
                sked_Bookable_days_in_advance__c = 10,
                sked_Booking_Type__c = skedConstants.SLOT_RESOURCE_TYPE,
                sked_Day__c = weekdays.get(i),
                sked_End_Time__c = 1800,
                sked_Market__c = regionId,
                sked_Number_of_Parallel_Bookings_Pending__c = 2,
                sked_Number_of_Parallel_bookings_Ready__c = 2,
                sked__Quantity__c = 1,
                sked_Start_Time__c = 800,
                sked_Dispatch_Time_before__c = 240
             );
            slots.add(slot);

            sked__Slot__c slot2 = slot.clone(false, false, false, false);
            slot.sked_Booking_Type__c = skedConstants.SLOT_UNALLOCATED_TYPE;
            slots.add(slot2);
        }

        insert slots;
    }

    public sked_Setter_Interface_Partner__c createSetterInterfacePartner(String partnerId, String interfaceId) {
        sked_Setter_Interface_Partner__c sip = new sked_Setter_Interface_Partner__c(
            sked_Partner__c = partnerId,
            sked_Setter_Interface_Version__c = interfaceId
        );

        insert sip;

        return sip;
    }
}
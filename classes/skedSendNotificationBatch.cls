public class skedSendNotificationBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{
	public String content;
    public Map<String, String> map_resId_sms;

    public skedSendNotificationBatch(String content) {
		this.content = content;
        this.map_resId_sms = new Map<String, String>();
	}

    public List<sObject> start(Database.BatchableContext context) {
        Integer position = content.indexOf('***');
        String resId = content.substring(0, position);
        String smsContent = content.substring(position + 3, content.length());
        this.map_resId_sms.put(resId, smsContent);
        List<sked__Resource__c> skedRes = [SELECT Id, sked__Mobile_Phone__c FROM sked__Resource__c WHERE id = : resId];

        return skedRes;
    }

    public void execute(Database.BatchableContext context, List<sObject> objects) {
        List<sked__Resource__c> skedRes = (List<sked__Resource__c>)objects;
        System.debug('1 in batch skedRes ' + skedRes);
        if (!skedRes.isEmpty()) {
            sked__Resource__c res = skedRes.get(0);
            System.debug('1 in batch res ' + res);

            if (this.map_resId_sms.containskey(res.id)) {
                String smsContent = this.map_resId_sms.get(res.id);
                skedSMS sms = new skedSMS();
                sms.tfrom = skedSmsServices.FROM_NUMBER;
                sms.to = skedSmsServices.generatePhone(res.sked__Mobile_Phone__c);
                sms.text = smsContent;
                string jsonformat = JSON.serialize(sms).replace('tfrom','from');
                System.debug('sms ' + sms);
                skedSmsServices.sendSMS(jsonformat);
            }
        }
    }

    public void finish(Database.BatchableContext context) {

    }
}
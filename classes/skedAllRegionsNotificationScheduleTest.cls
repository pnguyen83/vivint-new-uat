@isTest
public class skedAllRegionsNotificationScheduleTest {
	public static testMethod void doTest() {
        test.startTest();
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();
        try {
            skedAllRegionsNotificationSchedule abc = new skedAllRegionsNotificationSchedule(1);
            abc.execute(null);
        }
        catch(Exception ex) {

        }

        List<AsyncApexJob> asyncJobs = [select ApexClassId, Id, JobItemsProcessed, JobType,
                                                Status, NumberOfErrors, MethodName
                                        from AsyncApexJob
                                        where JobType in ('ScheduledApex')];
        System.debug('asyncJobs ' + asyncJobs.size());
        System.assert(asyncJobs.size() > 0);

        test.stopTest();
    }
}
global class skedAreaManagementQueueController {
    @remoteaction
    global static skedRemoteResultModel getAllJobs(skedVivintModel.AreaManagementFilter filter) {
        return skedAreaManagementQueueHandler.getJobsInRegion(filter);
    }

    @remoteaction
    global static skedRemoteResultModel getConfigData() {
        return skedAreaManagementQueueHandler.getConfigData();
    }

    @remoteaction
    global static skedRemoteResultModel cancelJob(skedVivintModel.VivintJob job) {
        return skedAreaManagementQueueHandler.cancelJob(job);
    }

    @remoteaction
    global static skedRemoteResultModel reassignJob(skedVivintModel.JobAvaiModel filter) {
       return skedAreaManagementQueueHandler.getResourcesToReassign(filter);
    }

    @remoteaction
    global static skedRemoteResultModel confirmAssignment(skedVivintModel.Assignment data) {
        return skedAreaManagementQueueHandler.confirmResourceAssignment(data);
    }

    @remoteaction
    global static skedRemoteResultModel getAllResources(skedVivintModel.AreaManagementFilter filter) {
       return  skedAreaManagementQueueHandler.getAllResourcesInRegions(filter);
    }

    @remoteaction
    global static skedRemoteResultModel getResourceAndJobsInRegions(skedVivintModel.AreaManagementFilter filter) {
        return  skedAreaManagementQueueHandler.getResAndJobInRegions(filter);
    }

    @remoteaction
    global static skedRemoteResultModel getResourcesForReschedule (skedVivintModel.AreaManagementFilter filter) {
        return skedAreaManagementQueueHandler.getResourceForReschedule(filter);
    }

    @remoteaction
    global static skedRemoteResultModel rescheduleJob(skedVivintModel.RescheduleJob data) {
        return skedAreaManagementQueueHandler.rescheduleJob(data);
    }

    @remoteaction
    global static skedRemoteResultModel confirmPendingDispatchJob(skedVivintModel.Appointment data) {
        return skedCloserHandler.confirmAppointment(data);
    }

    @remoteaction
    global static skedRemoteResultModel updateAttemptedJob(skedVivintModel.VivintJob job) {
        return skedCloserHandler.updateContactAttempTimeAndStatusOnJob(job);
    }

    @remoteaction
    global static skedRemoteResultModel getUnAllocatedJobs(skedVivintModel.AreaManagementFilter filter) {
        return skedAreaManagementQueueHandler.getUnAllocatedJobsForSchedule(filter);
    }

    @remoteaction
    global static skedRemoteResultModel updateJobInfo (skedVivintModel.VivintContact contact) {
        return skedCloserHandler.updateJobInformation(contact);
    }

    @remoteaction
    global static skedRemoteResultModel getSlotAndJobsMobile(skedVivintModel.AreaManagementFilter filter) {
        return skedAreaManagementQueueHandler.getSlotAndJobMobile(filter);
    }
}
public class skedCloserJobCreationCtrl {
    @remoteaction
    public static skedRemoteResultModel getBookingGrid(skedVivintModel.CloserFilterData data) {
        return skedCloserHandler.getCloserJobCreationGridData(data);
    }

    @remoteaction
    public static skedRemoteResultModel searchLocation(string name) {
        return skedSetterHandler.searchLocation(name);
    }

    @remoteaction
    public static skedRemoteResultModel saveJob(skedVivintModel.Appointment appointment) {
        appointment.fromCloserCreateJob = true;
        return skedSetterHandler.handleSaveAppointment(appointment);
    }
}